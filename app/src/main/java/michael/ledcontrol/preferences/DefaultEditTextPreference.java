/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.preferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

/**
 * EditTextPreference with onDialogClose Override
 *
 * @author Michael Schmitz
 */
public class DefaultEditTextPreference extends EditTextPreference {
    public DefaultEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public DefaultEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DefaultEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultEditTextPreference(Context context) {
        super(context);
    }


    /**
     * Calls the validation if the preference is saved
     *
     * @param positiveResult boolean that indicates how the view was closed
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(true);
    }
}
