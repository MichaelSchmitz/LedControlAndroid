/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.preferences;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;

/**
 * EditTextPreference with simple TextInput validation via TextWatcher to provide real time validation
 *
 * @author Michael Schmitz
 */
public class DeviceIdentifierTextPreference extends EditTextPreference {
    public DeviceIdentifierTextPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public DeviceIdentifierTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DeviceIdentifierTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DeviceIdentifierTextPreference(Context context) {
        super(context);
    }

    /**
     * Default showDialog with addition of the TextWatcher
     * @param state default param
     */
    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        getEditText().setError(null);
        getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                getEditText().setError(onValidate(s.toString()));
            }
        });
    }

    /**
     * Calls the validation if the preference is saved
     * @param positiveResult boolean that indicates how the view was closed
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        String errorMessage = onValidate(getEditText().getText().toString());
        if (errorMessage == null)
        {
            super.onDialogClosed(true);
        } else {
            super.onDialogClosed(false);
        }
    }

    /***
     * Called to validate contents of the edit text.
     *
     * Return null to indicate success, or return a validation error message to display on the edit text.
     *
     * @param text The text to validate.
     * @return An error message, or null if the value passes validation.
     */
    public String onValidate(String text)
    {
        if (text.length() == 5)
            return null;
        else
            return "Identifier not valid";
    }
}
