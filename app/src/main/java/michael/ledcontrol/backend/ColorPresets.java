/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * Class for managing the ColorPresets for the colorPicker
 *
 *  @author Michael Schmitz
 */
public class ColorPresets {
    private static ColorPresets instance = null;
    private int[] presets = new int[19];
    private List<String> keys = new ArrayList<>();
    private List<Integer> colors = new ArrayList<>();

    /**
     * Creates an instance if none exists, returns the instance
     * @param activity needed to instantiate ColorPresets
     * @return instance of ColorPresets
     */
    public static ColorPresets getInstance(MainActivity activity) {
        if (instance == null)
            instance = new ColorPresets(activity);
        return instance;
    }

    /**
     * Constructor adds the colorKeys and the corresponding default colors in separate lists
     * Triggers a update of the presets
     * @param activity needed to resolve colorResources
     */
    private ColorPresets(MainActivity activity) {
        for(int i = 1; i < 20; i++) {
            keys.add("color" + (i < 10 ? "0" : "") + i);
        }
        Resources res = activity.getResources();
        colors.add(res.getColor(R.color.presetRed, null));
        colors.add(res.getColor(R.color.presetPink, null));
        colors.add(res.getColor(R.color.presetLightPink, null));
        colors.add(res.getColor(R.color.presetPurple, null));
        colors.add(res.getColor(R.color.presetDeepPurple, null));
        colors.add(res.getColor(R.color.presetIndigo, null));
        colors.add(res.getColor(R.color.presetBlue, null));
        colors.add(res.getColor(R.color.presetLightBlue, null));
        colors.add(res.getColor(R.color.presetCyan, null));
        colors.add(res.getColor(R.color.presetTeal, null));
        colors.add(res.getColor(R.color.presetGreen, null));
        colors.add(res.getColor(R.color.presetLightGreen, null));
        colors.add(res.getColor(R.color.presetLime, null));
        colors.add(res.getColor(R.color.presetYellow, null));
        colors.add(res.getColor(R.color.presetAmber, null));
        colors.add(res.getColor(R.color.presetOrange, null));
        colors.add(res.getColor(R.color.presetBrown, null));
        colors.add(res.getColor(R.color.presetBlueGrey, null));
        colors.add(res.getColor(R.color.presetGrey, null));
        updatePresets(activity);
    }

    /**
     * loads for each colorPresets key the color saved in SharedPreferences, with the corresponding default value
     * @param activity needed to access the SharedPreferences
     */
    public void updatePresets(MainActivity activity) {
        for (int i = 0; i < presets.length; i++) {
            presets[i] = PreferenceManager.getDefaultSharedPreferences(activity).getInt(keys.get(i), colors.get(i));
        }
    }

    /**
     * Returns the presets
     * @return int array of Colors
     */
    public int[] getPresets() {
        return presets;
    }
}
