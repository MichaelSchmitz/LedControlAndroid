/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package michael.ledcontrol.backend.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import static michael.ledcontrol.backend.Database.Arduino.Power.On;
import static michael.ledcontrol.backend.Database.Arduino.Power.Standby;
import static michael.ledcontrol.backend.Database.Arduino.Power.noPowerOn;
import static michael.ledcontrol.backend.Database.Arduino.Power.noPowerStandby;

/**
 * Database schema for Room Database
 * Arduinos
 *
 * @author Michael Schmitz
 */
@Entity
public class Arduino {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public String ip;
    public String name;
    @TypeConverters(PowerConverter.class)
    public Power power;
    public int brightness;
    public String colour;

    @Ignore
    public boolean checked;

    /**
     * Used for generating new Arduino Objects with previous ID (for updating Arduinos)
     *
     * @param id         int ID
     * @param name       String name
     * @param ip         String IP
     * @param power      Power power
     * @param brightness int brightness
     * @param colour     String color
     */
    @Ignore
    public Arduino(int id, String name, String ip, Power power, int brightness, String colour) {
        this.id = id;
        this.name = name;
        this.ip = ip;
        this.power = power;
        this.brightness = brightness;
        this.colour = colour;
    }

    /**
     * Default constructor
     * @param name String name
     * @param ip String IP
     * @param power Power power
     * @param brightness int brightness
     * @param colour String color
     */
    public Arduino(String name, String ip, Power power, int brightness, String colour) {
        this.name = name;
        this.ip = ip;
        this.power = power;
        this.brightness = brightness;
        this.colour = colour;
    }

    public enum Power {
        noPowerOn,
        noPowerStandby,
        On,
        Standby
    }

    /**
     * Class to convert Power-Enum to String and reverse
     */
    public static class PowerConverter {

        @TypeConverter
        public Power fromString(String value) {
            switch (value) {
                case "noPowerOn":
                    return noPowerOn;
                case "noPowerStandy":
                    return noPowerStandby;
                case "On":
                    return On;
                case "Standby":
                    return Standby;
                default:
                    return On;
            }
        }

        @TypeConverter
        public String powerToString(Power power) {
            switch (power) {
                case noPowerOn:
                    return "noPowerOn";
                case noPowerStandby:
                    return "noPowerStandby";
                case On:
                    return "On";
                case Standby:
                    return "Standby";
                default:
                    return "";
            }
        }
    }

}