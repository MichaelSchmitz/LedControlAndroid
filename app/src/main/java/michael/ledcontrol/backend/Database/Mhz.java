/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Database schema for Room Database
 * 433Mhz receivers and Pi to send 433Mhz signals
 *
 * @author Michael Schmitz
 */
@Entity
public class Mhz {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String name;
    public String ip;
    public String net;
    public String device;
    @Ignore
    public boolean state;
    @Ignore
    public boolean checked;
    @Ignore
    public boolean serverUp;

    /**
     * Used for generating new Mhz Objects with previous ID (for updating Mhzs)
     *
     * @param id     previous id
     * @param name   Name
     * @param net    Net
     * @param device Device
     * @param ip     IP
     */
    @Ignore
    public Mhz(int id, String name, String net, String device, String ip) {
        this.id = id;
        this.name = name;
        this.net = net;
        this.device = device;
        this.ip = ip;
    }

    /**
     * Default constructor
     *
     * @param name   String describing the device
     * @param net    String of 5 chars either 0 or 1
     * @param device String of 5 chars either 0 or 1
     */
    public Mhz(String name, String net, String device, String ip) {
        this.name = name;
        this.net = net;
        this.device = device;
        this.ip = ip;
    }
}
