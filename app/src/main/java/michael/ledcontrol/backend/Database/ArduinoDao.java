/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Database Interface for Room Database
 *
 * @author Michael Schmitz
 */
@Dao
public interface ArduinoDao {
    @Query("SELECT * FROM Arduino")
    List<Arduino> getAll();

    @Query("SELECT * FROM Arduino WHERE ip LIKE :ip LIMIT 1")
    Arduino findByIp(String ip);

    @Query("SELECT * FROM Arduino WHERE id LIKE :id LIMIT 1")
    Arduino findById(int id);

    @Query("UPDATE Arduino SET 'power' = :value WHERE id LIKE :id")
    void updatePowerByID(String value, String id);

    @Query("UPDATE Arduino SET 'brightness' = :value WHERE id LIKE :id")
    void updateBrightnessByID(int value, int id);

    @Query("UPDATE Arduino SET 'colour' = :value WHERE id LIKE :id")
    void updateColourByID(String value, String id);

    @Query("UPDATE ARDUINO SET 'name' = :name , 'ip' = :ip , 'power' = :power ," +
            " 'brightness' = :brightness , 'colour' = :colour WHERE id LIKE :id")
    void replaceArduino(String name, String ip, String power, int brightness,
                        String colour, int id);

    @Insert
    void insert(Arduino arduino);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void updateArduinos(Arduino... Arduinos);

    @Delete
    void delete(Arduino Arduino);
}
