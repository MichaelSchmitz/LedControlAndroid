/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend.Database;

import android.arch.persistence.room.Room;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

import michael.ledcontrol.ui.main.MainActivity;


/**
 * Provides methods for the database Interface
 *
 * For further documentation please refer to the official Android Documentation specifically the RoomDatabase part
 *
 * @author Michael Schmitz
 */

public class DatabaseHelper {
    private static DatabaseHelper instance;
    private Database database;

    /**
     * Creates a new Instance if none exists returns the Helper
     * @param activity needed to instantiate the Helper
     * @return Instance of the Helper
     */
    public static DatabaseHelper getInstance(MainActivity activity) {
        if (instance == null)
            instance = new DatabaseHelper(activity);
        return instance;
    }
    /**
     * creates the database
     */
    private DatabaseHelper(MainActivity activity) {
        database = Room.databaseBuilder(activity,
                Database.class, "arduinoDB").fallbackToDestructiveMigration().build();
    }

    /**
     * inserts the Arduino, if the ip does not exist yet
     *
     * @param arduino Arduino to be added
     * @return success or failure of AsyncTask
     */
    public boolean addArduino(Arduino arduino) {
        try {
            if (getArduinoByIP(arduino.ip) == null) {
                new AddArduinoAsyncTask(database).execute(arduino);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * adds an arduino if its id does not exist yet
     *
     * @param arduino Arduino to be added
     * @return success or failure of AsyncTask
     */
    public boolean addArduinoByID(Arduino arduino) {
        try {
            if (getArduinoByID(arduino.id) == null) {
                new AddArduinoAsyncTask(database).execute(arduino);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * updates the given arduino
     *
     * @param arduino Arduino to be updated
     * @return false, if given arduino does not exist, otherwise success or failure of AsyncTask
     */
    public boolean updateArduino(Arduino arduino) {
        try {
            Arduino tmp = getArduinoByID(arduino.id);
            if (tmp != null) {
                new UpdateArduinoAsyncTask(database).execute(arduino);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Replaces the arduino in the second field with the one in the first
     *
     * @param arduinos in [0]: new arduino, in [1]: old arduino
     * @return success or failure of the AsyncTask
     */
    public boolean replaceArduino(Arduino[] arduinos) {
        try {
            new replaceArduinoAsyncTask(database).execute(arduinos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the powerSetting at the given id
     *
     * @param power new powerSetting
     * @param id    id of the row to be updated
     * @return success or failure of the AsyncTask
     */
    public boolean updatePower(Arduino.Power power, int id) {
        try {
            Arduino tmp = getArduinoByID(id);
            if (tmp != null) {
                new
                        updatePowerAsyncTask(database).execute(power.toString(), String.valueOf(id));
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the brightness at the given id
     *
     * @param brightness new brightness
     * @param id         id of the row to be updated
     * @return success or failure of the AsyncTask
     */
    public boolean updateBrightness(int brightness, int id) {
        try {
            Arduino tmp = getArduinoByID(id);
            if (tmp != null) {
                new
                        updateBrightnessAsyncTask(database).execute(brightness, id);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the color at the given id
     *
     * @param colour new color
     * @param id     id of the row
     * @return success or failure of the AsyncTask
     */
    public boolean updateColour(String colour, int id) {
        try {
            Arduino tmp = getArduinoByID(id);
            if (tmp != null) {
                new
                        updateColourAsyncTask(database).execute(colour, String.valueOf(id));
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * searches the database for an arduino with the given ip
     *
     * @param ip ip to be matched
     * @return Arduino object, if found or null
     */
    public Arduino getArduinoByIP(String ip) {
        Arduino arduino = null;
        try {
            arduino = new
                    getArduinoIPAsyncTask(database).execute(ip).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return arduino;
    }

    /**
     * searches the database for an arduino with the given id
     *
     * @param id ip to be matched
     * @return Arduino object, if found or null
     */
    public Arduino getArduinoByID(int id) {
        Arduino arduino = null;
        try {
            arduino = new
                    getArduinoIDAsyncTask(database).execute(id).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return arduino;
    }

    /**
     * Returns all arduinos
     *
     * @return List of arduinos if there are any in the database, else null
     */
    public List<Arduino> getArduinoData() {
        List<Arduino> arduino = null;
        try {
            arduino = new
                    getArduinosAsyncTask(database).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return arduino;
    }

    /**
     * deletes the given arduino from the database
     *
     * @param arduino arduino to delete
     * @return success or failure of the AsyncTask
     */
    public boolean deleteArduino(Arduino arduino) {
        Arduino tmp = getArduinoByIP(arduino.ip);
        if (tmp != null) {
            new deleteArduinoAsyncTask(database).execute(arduino);
            return true;
        }
        return false;
    }

    /**
     * Returns all Mhz devices
     * @return List of devices or null, if none are in the database
     */
    public List<Mhz> getAllMhzs() {
        List<Mhz> mhzs = null;
        try {
            mhzs = new
                    getAllMhzsAsyncTask(database).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return mhzs;
    }

    /**
     * Gets a Mhz device by its id
     * @param id id to query
     * @return Mhz device
     */
    public Mhz getMhzByID(int id) {
        Mhz mhz = null;
        try {
            mhz = new
                    getMhzByIDAsyncTask(database).execute(id).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return mhz;
    }

    /**
     * Gets a Mhz device by its net and device specifier
     * @param device device string to query
     * @param net net string to query
     * @return Mhz device
     */
    public Mhz getMhzByDeviceAndNet(String device, String net) {
        Mhz mhz = null;
        try {
            mhz = new
                    getMhzByDeviceAndNetAsyncTask(database).execute(device, net).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return mhz;
    }

    /**
     * Updates the device identifier
     * @param value new identifier
     * @param id position to update
     * @return Success or failure of the AsyncTask
     */
    public boolean updateDevice(String value, int id) {
        try {
            new updateDeviceAsyncTask(database).execute(value, String.valueOf(id));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the IP address
     * @param value new IP adress
     * @param id position to update
     * @return Success or failure of the AsyncTask
     */
    public boolean updateMhzIP(String value, int id) {
        try {
            new updateMhzIPAsyncTask(database).execute(value, String.valueOf(id));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the net identifier
     * @param value new identifier
     * @param id position to update
     * @return Success or failure of the AsyncTask
     */
    public boolean updateNet(String value, int id) {
        try {
            new updateNetAsyncTask(database).execute(value, String.valueOf(id));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Updates the given Mhz device
     * @param mhz Mhz device to update
     * @return Success or failure of the AsyncTask
     */
    public boolean updateMhz(Mhz mhz) {
        try {
            Mhz tmp = getMhzByID(mhz.id);
            if (tmp != null) {
                new UpdateMhzAsyncTask(database).execute(mhz);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * replaces the Mhz device at pos 1 with the Mhz device at pos 0
     * @param mhzs Array with two Mhz devices
     * @return Success or failure of AsyncTask
     */
    public boolean replaceMhz(Mhz[] mhzs) {
        try {
            new replaceMhzAsyncTask(database).execute(mhzs);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Adds a new Mhz device to the database
     * @param mhz Mhz device to add
     * @return Success or failure of the AsyncTask
     */
    public boolean addMhz(Mhz mhz) {
        try {
            if (getMhzByDeviceAndNet(mhz.device, mhz.net) == null) {
                new AddMhzAsyncTask(database).execute(mhz);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Deletes the given Mhz device from the database
     * @param mhz Mhz device to delete
     * @return Success or failure of the AsyncTask
     */
    public boolean deleteMhz(Mhz mhz) {
        Mhz tmp = getMhzByDeviceAndNet(mhz.device, mhz.net);
        if (tmp != null) {
            new deleteMhzAsyncTask(database).execute(mhz);
            return true;
        }
        return false;
    }

    /**
     * AsyncTask for adding a single Arduino in the database
     */
    private static class AddArduinoAsyncTask extends AsyncTask<Arduino, Void, Void> {
        private Database database;

        private AddArduinoAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Arduino... arduinos) {
            database.arduinoDao().insert(arduinos[0]);
            return null;
        }
    }

    /**
     * AsyncTask for updating a single Arduino in the database
     */
    private static class UpdateArduinoAsyncTask extends AsyncTask<Arduino, Void, Void> {
        private Database database;

        private UpdateArduinoAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Arduino... Arduinos) {
            database.arduinoDao().updateArduinos(Arduinos);
            return null;
        }
    }

    /**
     * AsyncTask for updating the power for a specific row
     */
    private static class updatePowerAsyncTask extends AsyncTask<String, Void, Void> {
        private Database database;

        private updatePowerAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(String... strings) {
            database.arduinoDao().updatePowerByID(strings[0], strings[1]);
            return null;
        }
    }

    /**
     * AsyncTask for updating the color in a specific row
     */
    private static class updateColourAsyncTask extends AsyncTask<String, Void, Void> {
        private Database database;

        private updateColourAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(String... strings) {
            database.arduinoDao().updateColourByID(strings[0], strings[1]);
            return null;
        }
    }

    /**
     * AsyncTask for updating the brightness in a specific row
     */
    private static class updateBrightnessAsyncTask extends AsyncTask<Integer, Void, Void> {
        private Database database;

        private updateBrightnessAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Integer... params) {
            database.arduinoDao().updateBrightnessByID(params[0], params[1]);
            return null;
        }
    }

    /**
     * AsyncTask for querying an Arduino based on its ip
     */
    private static class getArduinoIPAsyncTask extends AsyncTask<String, Void, Arduino> {
        private Database database;

        private getArduinoIPAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Arduino doInBackground(String... name) {
            return database.arduinoDao().findByIp(name[0]);
        }

        @Override
        protected void onPostExecute(Arduino arduino) {
            super.onPostExecute(arduino);
        }
    }

    /**
     * AsyncTask for querying an Arduino by its id
     */
    private static class getArduinoIDAsyncTask extends AsyncTask<Integer, Void, Arduino> {
        private Database database;

        private getArduinoIDAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Arduino doInBackground(Integer... id) {
            return database.arduinoDao().findById(id[0]);
        }

        @Override
        protected void onPostExecute(Arduino arduino) {
            super.onPostExecute(arduino);
        }
    }

    /**
     * AsyncTask for querying all Arduinos except the one with id 1
     */
    private static class getArduinosAsyncTask extends AsyncTask<Void, Void, List<Arduino>> {

        private Database database;

        private getArduinosAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected List<Arduino> doInBackground(Void... params) {
            return database.arduinoDao().getAll();
        }

        @Override
        protected void onPostExecute(List<Arduino> arduino) {
            super.onPostExecute(arduino);
        }
    }

    /**
     * AsyncTask for deleting a specific Arduino
     */
    private static class deleteArduinoAsyncTask extends AsyncTask<Arduino, Void, Void> {
        private Database database;

        private deleteArduinoAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Arduino... arduino) {
            database.arduinoDao().delete(arduino[0]);
            return null;
        }
    }

    /**
     * AsyncTask for updating a row with completly new values (replacing the row)
     */
    private static class replaceArduinoAsyncTask extends AsyncTask<Arduino[], Void, Void> {
        private Database database;

        private replaceArduinoAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Arduino[]... arduinos) {
            database.arduinoDao().replaceArduino(arduinos[0][0].name, arduinos[0][0].ip,
                    arduinos[0][0].power.toString(), arduinos[0][0].brightness, arduinos[0][0].colour, arduinos[0][1].id);
            return null;
        }
    }

    /**
     * AsyncTask for querying all Mhz except the one with id 1
     */
    private static class getAllMhzsAsyncTask extends AsyncTask<Void, Void, List<Mhz>> {

        private Database database;

        private getAllMhzsAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected List<Mhz> doInBackground(Void... params) {
            return database.mhzDao().getAll();
        }

        @Override
        protected void onPostExecute(List<Mhz> mhz) {
            super.onPostExecute(mhz);
        }
    }

    /**
     * AsyncTask for deleting a specific Mhz
     */
    private static class deleteMhzAsyncTask extends AsyncTask<Mhz, Void, Void> {
        private Database database;

        private deleteMhzAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Mhz... mhzs) {
            database.mhzDao().delete(mhzs[0]);
            return null;
        }
    }

    /**
     * AsyncTask for updating the Net in a specific row
     */
    private static class updateNetAsyncTask extends AsyncTask<String, Void, Void> {
        private Database database;

        private updateNetAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(String... strings) {
            database.mhzDao().updateNetByID(strings[0], Integer.parseInt(strings[1]));
            return null;
        }
    }

    /**
     * AsyncTask for updating the Device in a specific row
     */
    private static class updateDeviceAsyncTask extends AsyncTask<String, Void, Void> {
        private Database database;

        private updateDeviceAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(String... params) {
            database.mhzDao().updateDeviceByID(params[0], Integer.parseInt(params[1]));
            return null;
        }
    }

    /**
     * AsyncTask for updating the Device in a specific row
     */
    private static class updateMhzIPAsyncTask extends AsyncTask<String, Void, Void> {
        private Database database;

        private updateMhzIPAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(String... params) {
            database.mhzDao().updateIPByID(params[0], Integer.parseInt(params[1]));
            return null;
        }
    }

    /**
     * AsyncTask for querying an MhzDevice based on its Device and Net
     */
    private static class getMhzByDeviceAndNetAsyncTask extends AsyncTask<String, Void, Mhz> {
        private Database database;

        private getMhzByDeviceAndNetAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Mhz doInBackground(String... name) {
            return database.mhzDao().findByDeviceAndNet(name[0], name[1]);
        }

        @Override
        protected void onPostExecute(Mhz mhz) {
            super.onPostExecute(mhz);
        }
    }

    /**
     * AsyncTask for querying an MhzDevice by its id
     */
    private static class getMhzByIDAsyncTask extends AsyncTask<Integer, Void, Mhz> {
        private Database database;

        private getMhzByIDAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Mhz doInBackground(Integer... id) {
            return database.mhzDao().findById(id[0]);
        }

        @Override
        protected void onPostExecute(Mhz mhz) {
            super.onPostExecute(mhz);
        }
    }

    /**
     * AsyncTask for updating a row with completly new values (replacing the row)
     */
    private static class replaceMhzAsyncTask extends AsyncTask<Mhz[], Void, Void> {
        private Database database;

        private replaceMhzAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Mhz[]... mhzs) {
            database.mhzDao().replaceMhz(mhzs[0][0].name, mhzs[0][0].ip,
                    mhzs[0][0].device, mhzs[0][0].net, mhzs[0][1].id);
            return null;
        }
    }

    /**
     * AsyncTask for updating a single Mhz in the database
     */
    private static class UpdateMhzAsyncTask extends AsyncTask<Mhz, Void, Void> {
        private Database database;

        private UpdateMhzAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Mhz... mhzs) {
            database.mhzDao().updateMhzs(mhzs);
            return null;
        }
    }

    /**
     * AsyncTask for adding a single Mhz in the database
     */
    private static class AddMhzAsyncTask extends AsyncTask<Mhz, Void, Void> {
        private Database database;

        private AddMhzAsyncTask(Database database) {
            this.database = database;
        }

        @Override
        protected Void doInBackground(Mhz... mhzs) {
            database.mhzDao().insert(mhzs[0]);
            return null;
        }
    }
}

