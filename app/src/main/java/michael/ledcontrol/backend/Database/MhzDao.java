/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Database Interface for Room Database
 *
 * @author Michael Schmitz
 */
@Dao
public interface MhzDao {
    @Query("SELECT * FROM Mhz")
    List<Mhz> getAll();

    @Query("SELECT * FROM Mhz WHERE id LIKE :id LIMIT 1")
    Mhz findById(int id);

    @Query("SELECT * FROM Mhz WHERE net = :net AND device = :device LIMIT 1")
    Mhz findByDeviceAndNet(String net, String device);

    @Query("UPDATE Mhz SET 'device' = :value WHERE id LIKE :id")
    void updateDeviceByID(String value, int id);

    @Query("UPDATE Mhz SET 'net' = :value WHERE id LIKE :id")
    void updateNetByID(String value, int id);

    @Query("UPDATE Mhz SET 'ip' = :value WHERE id LIKE :id")
    void updateIPByID(String value, int id);

    @Query("UPDATE Mhz SET 'name' = :name , 'ip' = :ip , 'net' = :net ," +
            " 'device' = :device WHERE id LIKE :id")
    void replaceMhz(String name, String ip, String net, String device, int id);

    @Insert
    void insert(Mhz mhz);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void updateMhzs(Mhz... Mhzs);

    @Delete
    void delete(Mhz mhz);
}
