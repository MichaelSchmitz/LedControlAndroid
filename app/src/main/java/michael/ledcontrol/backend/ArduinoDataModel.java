/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import java.util.ArrayList;
import java.util.List;

import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Database.DatabaseHelper;
import michael.ledcontrol.ui.arduino.ArduinoRecyclerView;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * ViewModel for Arduinos, includes function to update the list and simultaneously the Database
 *
 * IMPORTANT: Set the RecyclerView in the onCreate where the View is created
 *
 * @author Michael Schmitz
 */
public class ArduinoDataModel {
    private static ArduinoDataModel instance;
    private DatabaseHelper databaseHelper;
    private List<Arduino> arduinos;
    private List<Network> networks;
    private ArduinoRecyclerView recyclerView;

    /**
     * Creates an instance of ArduinoDataModel if none exists, returns the instance
     * @param activity needed to instantiate ArduinoDataModel
     * @return instance of ArduinoDataModel
     */
    public static ArduinoDataModel getInstance(MainActivity activity) {
        if (instance == null)
            instance = new ArduinoDataModel(activity);
        return instance;
    }

    /**
     * Constructor which sets the DatabaseHelper
     * @param activity MainActivity
     */
    private ArduinoDataModel(MainActivity activity) {
        databaseHelper = DatabaseHelper.getInstance(activity);
    }

    /**
     * Sets the ArduinoRecyclerView, needed for updating the RecycleView
     *
     * @param arduinoRecyclerView ArduinoRecyclerView of the RecycleView
     */
    public void setRecyclerView(ArduinoRecyclerView arduinoRecyclerView) {
        recyclerView = arduinoRecyclerView;
    }

    /**
     * Returns all Arduinos currently in the List, if none are specified up to now, a new Database-Query is made
     *
     * @return List of Arduinos
     */
    public List<Arduino> getArduinos() {
        if (arduinos == null)
            arduinos = databaseHelper.getArduinoData();
        return arduinos;
    }

    /**
     * Returns all Network-Instances currently in the List, if none are specified up to now, all are generated
     *
     * @return List of Network Instances
     */
    public List<Network> getNetworks() {
        if (networks == null || networks.size() == 0) {
            networks = new ArrayList<>();
            for (int i = 0; i < getArduinos().size(); i++) {
                networks.add(new Network(getArduinos().get(i)));
            }
        }
        return networks;
    }

    /**
     * Adds an new Arduino to the RecycleView and Database, notifies the View of the change
     *
     * @param arduino Arduino to add
     * @return Success or failure of Database-Operation
     */
    public boolean add(Arduino arduino) {
        if (arduinos.contains(arduino))
            return false;
        arduinos.add(arduino);
        getNetworks().add(new Network(arduino));
        int pos = arduinos.size() - 1;
        sendMsg(getNetworks().get(pos), arduinos.get(pos));
        recyclerView.notifyItemInserted(pos);
        return databaseHelper.addArduino(arduino);
    }

    /**
     * Adds an Arduino to the RecycleView at the given positon and reorders the Database
     * notifies the View of the changes
     *
     * @param arduino Arduino to add
     * @param pos     Position at which the arduino is to add
     * @return Success or failure of Database-Operation
     */
    public boolean addAtPos(Arduino arduino, int pos) {
        if (arduinos.contains(arduino))
            return false;
        arduinos.add(pos, arduino);
        getNetworks().add(pos, new Network(arduino));
        sendMsg(getNetworks().get(pos), arduinos.get(pos));
        recyclerView.notifyItemInserted(pos);
        return databaseHelper.addArduino(arduino) && reorderDatabase();
    }

    /**
     * Updates an Arduino in the RecycleView and Database, notifies the View of the change
     *
     * @param arduino Arduino to update
     * @param pos     Position of Arduino in the list
     * @return Success or failure of Database-Operation
     */
    public boolean update(Arduino arduino, int pos) {
        arduinos.set(pos, arduino);
        getNetworks().set(pos, new Network(arduino));
        sendMsg(getNetworks().get(pos), arduinos.get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateArduino(arduino);
    }

    /**
     * Updates the Power value of a specific Arduino, notifies the View of the change
     *
     * @param power new Power value
     * @param pos   Position of Arduino to update in the list
     * @return Success or failure of Database-Operation
     */
    public boolean setPower(Arduino.Power power, int pos) {
        arduinos.get(pos).power = power;
        sendMsg(getNetworks().get(pos), arduinos.get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updatePower(power, arduinos.get(pos).id);
    }

    /**
     * Updates the Brightness value of a specific Arduino, notifies the View of the change
     *
     * @param brightness new Brightness value
     * @param pos        Position of Arduino to update in the list
     * @return Success or failure of Database-Operation
     */
    public boolean setBrightness(int brightness, int pos) {
        arduinos.get(pos).brightness = brightness;

        System.out.println(getNetworks().get(pos));
        sendMsg(getNetworks().get(pos), getArduinos().get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateBrightness(brightness, getArduinos().get(pos).id);
    }

    /**
     * Updates the ColorConverter value of a specific Arduino, notifies the View of the change
     *
     * @param colour new ColorConverter value
     * @param pos    Position of Arduino to update in the list
     * @return Success or failure of Database-Operation
     */
    public boolean setColour(String colour, int pos) {
        getArduinos().get(pos).colour = colour;
        sendMsg(getNetworks().get(pos), getArduinos().get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateColour(colour, getArduinos().get(pos).id);
    }

    /**
     * Writes the current state of the arduinoList in the database
     *
     * @return if all entrys are written true
     */
    public boolean reorderDatabase() {
        int erg = 0;
        List<Arduino> tmp = databaseHelper.getArduinoData();
        for (int i = 0; i < arduinos.size(); i++) {
            if (databaseHelper.replaceArduino(new Arduino[]{arduinos.get(i), tmp.get(i)}))
                erg++;
        }
        return erg == arduinos.size();
    }

    /**
     * Deletes an Arduino out of the list and Database and notifies the View of the change
     *
     * @param pos position of Arduino to delete
     * @return Success or failure of Database-Operation
     */
    public boolean delete(int pos) {
        Arduino tmp = getArduinos().get(pos);
        getArduinos().remove(pos);
        getNetworks().remove(pos);
        recyclerView.notifyItemRemoved(pos);
        return databaseHelper.deleteArduino(tmp);
    }

    /**
     * Returns an list in which the positions of checked arduinos are hold
     *
     * @return list of positions
     */
    public List<Integer> whichAreChecked() {
        List<Integer> tmp = new ArrayList<>(0);
        if (arduinos != null) {
            for (int i = 0; i < arduinos.size(); i++) {
                if (isChecked(i)) {
                    tmp.add(i);
                }
            }
        }
        return tmp;
    }

    /**
     * returns the status of an Arduino at the given position
     *
     * @param pos position of Arduino to check
     * @return true, if Arduino was selected
     */
    public boolean isChecked(int pos) {
        return getArduinos().get(pos).checked;
    }

    /**
     * sets the Arduino checked param to the given value
     *
     * @param toSet value to set
     * @param pos   position of Arduino to set
     */
    public void setChecked(boolean toSet, int pos) {
        getArduinos().get(pos).checked = toSet;
        recyclerView.notifyItemChanged(pos);
    }

    /**
     * Sends a message accordingly to the given Arduino Object, it differences 3 cases: Arduino Standby, Color, and Mode
     *
     * @param network Network Object used to send the message
     * @param arduino Arduino Object with information to send
     */
    private void sendMsg(Network network, Arduino arduino) {
        if (arduino.power == Arduino.Power.noPowerOn || arduino.power == Arduino.Power.noPowerStandby)
            return;
        int[] msg = new int[8];
        int sum = 0;
        msg[0] = 0xBB;
        if (arduino.power == (Arduino.Power.Standby)) {
            msg[1] = 0xAA;
        } else if (arduino.colour.startsWith("#")) {
            msg[1] = 0x11;
            msg[2] = arduino.brightness;
            msg[3] = Integer.parseInt(arduino.colour.substring(1, 3), 16);
            msg[4] = Integer.parseInt(arduino.colour.substring(3, 5), 16);
            msg[5] = Integer.parseInt(arduino.colour.substring(5, 7), 16);
        } else {
            msg[1] = 0x22;
            msg[2] = arduino.brightness;
            switch (arduino.colour) {
                case "rainbow": {
                    msg[3] = 1;
                    break;
                }
                case "rainbow_circle": {
                    msg[3] = 3;
                    break;
                }
                case "rainbow_sinus": {
                    msg[3] = 2;
                    break;
                }
                case "spectrum": {
                    msg[3] = 4;
                    break;
                }
                case "spectrum_sinus": {
                    msg[3] = 5;
                    break;
                }
            }
        }
        for (int i = 0; i < 6; i++) {
            sum += msg[i];
        }
        msg[6] = sum >> 8;
        msg[7] = sum & 0x00FF;
        network.sendMessage(msg);
    }
}
