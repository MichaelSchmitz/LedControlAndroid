/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import michael.ledcontrol.backend.Database.DatabaseHelper;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.mhz.MhzRecyclerView;

/**
 * ViewModel for Mhz devices, includes function to update the list and simultaneously the Database
 *
 * IMPORTANT: Set the RecyclerView in the onCreate where the View is created
 *
 * @author Michael Schmitz
 */
public class MhzDataModel {
    private static MhzDataModel instance;
    private DatabaseHelper databaseHelper;
    private List<Mhz> mhzs;
    private Map<String, Network> networks;
    private MhzRecyclerView recyclerView;

    /**
     * Creates an instance of MhzDataModel if none exists, returns the instance
     * @param activity needed to instantiate MhzDataModel
     * @return instance of MhzDataModel
     */
    public static MhzDataModel getInstance(MainActivity activity) {
        if (instance == null)
            instance = new MhzDataModel(activity);
        return instance;
    }

    /**
     * Constructor wich sets the DatabaseHelper
     * @param activity needed to get the instance of the DatabaseHelper
     */
    private MhzDataModel(MainActivity activity) {
        databaseHelper = DatabaseHelper.getInstance(activity);
    }


    /**
     * Sets the MhzRecyclerView, needed for updating the RecycleView
     *
     * @param mhzRecyclerView MhzRecyclerView of the RecycleView
     */
    public void setRecyclerView(MhzRecyclerView mhzRecyclerView) {
        recyclerView = mhzRecyclerView;
    }


    /**
     * Returns all MhzDevices currently in the List, if none are specified up to now, a new Database-Query is made
     *
     * @return List of MhzDevices
     */
    public List<Mhz> getMhzs() {
        if (mhzs == null)
            mhzs = databaseHelper.getAllMhzs();
        return mhzs;
    }


    /**
     * Returns all Mhz Network-Instances currently in the List, if none are specified up to now, all are generated
     *
     * @return List of Network Instances
     */
    public Map<String, Network> getNetworks() {
        if (networks == null || networks.size() == 0) {
            networks = new LinkedHashMap<>();
            for (int i = 0; i < getMhzs().size(); i++) {
                Mhz mhz = getMhzs().get(i);
                networks.put(mhz.ip, new Network(mhz));
            }
        }
        return networks;
    }

    /**
     * Writes the current state of the mhzList in the database
     *
     * @return if all entrys are written true
     */
    public boolean reorderDatabase() {
        int erg = 0;
        List<Mhz> tmp2 = databaseHelper.getAllMhzs();
        for (int i = 0; i < mhzs.size(); i++) {
            if (databaseHelper.replaceMhz(new Mhz[]{mhzs.get(i), tmp2.get(i)}))
                erg++;
        }
        return erg == mhzs.size();
    }

    /**
     * Returns an list in which the positions of checked mhzs are hold
     *
     * @return list of positions
     */
    public List<Integer> whichAreChecked() {
        List<Integer> tmp = new ArrayList<>(0);
        if (mhzs != null) {
            for (int i = 0; i < mhzs.size(); i++) {
                if (isChecked(i)) {
                    tmp.add(i);
                }
            }
        }
        return tmp;
    }

    /**
     * returns the status of an Mhz at the given position
     *
     * @param pos position of Mhz to check
     * @return true, if Mhz was selected
     */
    public boolean isChecked(int pos) {
        return getMhzs().get(pos).checked;
    }

    /**
     * sets the Mhz checked param to the given value
     *
     * @param toSet value to set
     * @param pos   position of Mhz to set
     */
    public void setChecked(boolean toSet, int pos) {
        getMhzs().get(pos).checked = toSet;
        recyclerView.notifyItemChanged(pos);
    }

    /**
     * sets the net identifier at the given position
     * @param net new net identifier
     * @param pos position to update
     * @return Success or failure of database operation
     */
    public boolean setNet(String net, int pos) {
        getMhzs().get(pos).net = net;
        sendMsg(getNetworks().get(getMhzs().get(pos).ip), getMhzs().get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateNet(net, getMhzs().get(pos).id);
    }

    /**
     * sets the device identifier at the given position
     * @param device new device identifier
     * @param pos position to update
     * @return Success or failure of database operation
     */
    public boolean setDevice(String device, int pos) {
        getMhzs().get(pos).device = device;
        sendMsg(getNetworks().get(getMhzs().get(pos).ip), getMhzs().get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateDevice(device, getMhzs().get(pos).id);
    }

    /**
     * sets the power state at the given position
     * @param power power state
     * @param pos position to update
     * @return if position is valid true, else false
     */
    public boolean setPower(boolean power, int pos) {
        if (pos == -2)
            return false;
        if (getMhzs().get(pos).state != power) {
            getMhzs().get(pos).state = power;
            sendMsg(getNetworks().get(getMhzs().get(pos).ip), getMhzs().get(pos));
            if (recyclerView != null)
                recyclerView.notifyItemChanged(pos);
        }
        return true;
    }

    /**
     * sets the ip at the given position
     * @param ip new ip
     * @param pos position to update
     * @return Success or failure of database operation
     */
    public boolean setIP(String ip, int pos) {
        getMhzs().get(pos).ip = ip;
        if (!getNetworks().containsKey(ip)) {
            getNetworks().put(ip, new Network(getMhzs().get(pos)));
        }
        sendMsg(getNetworks().get(getMhzs().get(pos).ip), getMhzs().get(pos));
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateMhzIP(ip, getMhzs().get(pos).id);
    }

    /**
     * Updates the Mhz device at the given position
     * @param mhz Mhz device to update
     * @param pos position to update
     * @return Success or failure of database operation
     */
    public boolean update(Mhz mhz, int pos) {
        getMhzs().set(pos, mhz);
        recyclerView.notifyItemChanged(pos);
        return databaseHelper.updateMhz(mhz);
    }

    /**
     * Adds an new 433Mhz device to the RecycleView and Database, notifies the View of the change
     *
     * @param mhz Mhz device to add
     * @return Success or failure of database operation
     */
    public boolean add(Mhz mhz) {
        if (mhzs.contains(mhz))
            return false;
        mhzs.add(mhz);
        if (!getNetworks().containsKey(mhz.ip))
            getNetworks().put(mhz.ip, new Network(mhz));
        int pos = mhzs.size() - 1;
        recyclerView.notifyItemInserted(pos);
        return databaseHelper.addMhz(mhz);
    }

    /**
     * Adds an 433Mhz device to the RecycleView at the given positon and reorders the Database
     * notifies the View of the changes
     *
     * @param mhz Mhz device to add
     * @param pos Position at which the arduino is to add
     * @return Success or failure of database operation
     */
    public boolean addAtPos(Mhz mhz, int pos) {
        if (mhzs.contains(mhz))
            return false;
        mhzs.add(pos, mhz);
        if (!getNetworks().containsKey(mhz.ip))
            getNetworks().put(mhz.ip, new Network(mhz));
        recyclerView.notifyItemInserted(pos);
        return databaseHelper.addMhz(mhz) && reorderDatabase();
    }


    /**
     * Deletes an Mhz device out of the list and Database and notifies the View of the change
     *
     * @param pos position of Mhz to delete
     * @return Success or failure of database operation
     */
    public boolean delete(int pos) {
        Mhz tmp = getMhzs().get(pos);
        getMhzs().remove(pos);
        recyclerView.notifyItemRemoved(pos);
        return databaseHelper.deleteMhz(tmp);
    }

    /**
     * Updates the server state for each Mhz device that is bound to this ip
     * @param ip IP of the server
     * @param state new state
     */
    public void updateServerState(String ip, boolean state) {
        if (recyclerView != null) {
            List<Mhz> mhzs = getMhzs();
            for (int i = 0; i < mhzs.size(); i++) {
                if (mhzs.get(i).ip.equals(ip)) {
                    if (mhzs.get(i).serverUp != state) {
                        mhzs.get(i).serverUp = state;
                        recyclerView.notifyItemChanged(i);
                    }
                }
            }
        }
    }

    /**
     * Querys the position of a Mhz device by its net and device identifiers
     * @param net net identifier
     * @param device device identifier
     * @return position of device
     */
    public int getPos(int net, int device) {
        List<Mhz> mhzs = getMhzs();
        for (int i = 0; i < mhzs.size(); i++) {
            if (net == Integer.parseInt(mhzs.get(i).net, 2) && device == Integer.parseInt(mhzs.get(i).device, 2))
                return i;
        }
        return -2;
    }

    /**
     * Sends a message accordingly to the given Mhz Object
     *
     * @param network Network Object used to send the message
     * @param mhz     Mhz Object with information to send
     */
    private void sendMsg(Network network, Mhz mhz) {
        int[] msg = new int[6];
        int sum = 0;
        msg[0] = 0xCC;
        if (mhz.state) {
            msg[1] = 0xAA;
        } else {
            msg[1] = 0xBB;
        }
        msg[2] = Integer.parseInt(mhz.net, 2);
        msg[3] = Integer.parseInt(mhz.device, 2);
        for (int i = 0; i < 6; i++) {
            sum += msg[i];
        }
        msg[4] = sum >> 8;
        msg[5] = sum & 0x00FF;
        network.sendMessage(msg);
    }
}
