/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Database.Mhz;

/**
 * Class that provides functionality for network communication to the Arduinos.
 * For each Arduino one instance should be created.
 *
 * @author Michael Schmitz
 */

public class Network {
    private int server_port;
    private InetAddress local;
    private static DatagramSocket s = null;

    /**
     * Sets the connection params according to given param
     *
     * @param arduino Arduino object of Arduino to whom a connection should be established
     */
    public Network(Arduino arduino) {
        String[] tmp = arduino.ip.split(":");
        if (tmp.length == 1)
            server_port = 1234;
        else
            server_port = Integer.parseInt(tmp[1]);
        try {
            local = InetAddress.getByName(tmp[0]);
            s = new DatagramSocket();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the connection params according to given param
     *
     * @param mhz Mhz object of 433Mhz server to whom a connection should be established
     */
    public Network(Mhz mhz) {
        String[] tmp = mhz.ip.split(":");
        if (tmp.length == 1)
            server_port = 1234;
        else
            server_port = Integer.parseInt(tmp[1]);
        try {
            local = InetAddress.getByName(tmp[0]);
            if (s == null)
                s = new DatagramSocket();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }
    }

    /**
     * Validates the given IP-String, checks for correct format (x.x.x.x[:y]) and if the values are in the correct area (0 < x < 256 && 0 < y < 65635)
     *
     * @param ip IP to validate
     * @return true if IP is correct
     */
    public static boolean validateIP(String ip) {
        String[] port = ip.split(":");
        if (port.length > 2 || port.length == 0)
            return false;
        else {
            if (port.length > 1) {
                if (port[1].split("\\.").length > 1)
                    return false;
                else if (Integer.parseInt(port[1]) > 65535 || Integer.parseInt(port[1]) == 0)
                    return false;
            }
            String[] ipParts = port[0].split("\\.");
            if (ipParts.length != 4)
                return false;
            else {
                for (String part : ipParts) {
                    if (Integer.parseInt(part) > 255 || Integer.parseInt(part) == 0)
                        return false;
                }
                return true;
            }
        }

    }

    /**
     * Returns the IP
     * @return InetAdress of the instance
     */
    public InetAddress getLocal() {
        return local;
    }

    /**
     * Returns the UDP Socket (for the Listener)
     * @return DatagramSocket (UDP)
     */
    protected static DatagramSocket getSocket()  {
        return s;
    }

    /**
     * Sends a message in a new Thread
     * @param message int[] contains the message (Range of ints: 0 ... 255)
     */
    public void sendMessage(final int[] message) {
        ExecutorService exec = Executors.newSingleThreadExecutor();
        Runnable sendRunnable = new Runnable() {
            @Override
            public void run() {
                int msg_length = message.length;
                byte[] msg = toByte(message);
                if (msg_length == 8 || msg_length == 6) {
                    DatagramPacket p = new DatagramPacket(msg, msg_length, local, server_port);
                    try {
                        s.send(p);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        exec.execute(sendRunnable);
    }

    /**
     * Helper method for send method.
     * Converts the given int[] to a byte[]
     * Validates if the Integer is in a valid range for unsigned byte.
     *
     * Needed as Java does not supplies a unsigned byte data type
     * @param input int[] Integers should be between 0 and 255
     * @return converted byte[]
     */
    private byte[] toByte(int[] input) {
        byte[] output = new byte[input.length];
        for (int i = 0; i < input.length; i++) {
            if (input[i] > 0 && input[i] < 256)
                output[i] = (byte) input[i];
        }
        return output;
    }
}

