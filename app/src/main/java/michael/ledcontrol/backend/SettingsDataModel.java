/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * ViewModel for the preferences configured in the preference screen
 *
 * @author Michael Schmitz
 */
public class SettingsDataModel {
    private static SettingsDataModel instance;
    private int refreshrate;
    @ColorInt private int colour;
    private Arduino arduino;
    private Mhz mhz;
    private int[] colourPresets;

    /**
     * Creates an instance of SettingsDataModel if none exists, returns the instance
     *
     * @param activity needed to instantiate SettingsDataModel
     * @return instance of SettingsDataModel
     */
    public static SettingsDataModel getInstance(MainActivity activity) {
        if (instance == null)
            instance = new SettingsDataModel(activity);
        return instance;
    }

    /**
     * Constructor
     * Loads all preferences with the corresponding default values
     * @param activity needed to access SharedPreferences
     */
    private SettingsDataModel(MainActivity activity) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        refreshrate = Integer.parseInt(sharedPreferences.getString("refreshrate", "15"));
        colour = sharedPreferences.getInt("primaryColour", activity.getColor(R.color.presetGreen));
        String arduinoName = sharedPreferences.getString("arduino_name", "default");
        String arudinoIP = sharedPreferences.getString("arudino_ip", "192.168.178.1");
        int arduinoColor = sharedPreferences.getInt("arduino_color", activity.getColor(R.color.presetGreen));
        boolean arduinoPower = sharedPreferences.getBoolean("arduino_power", true);
        int arduinoBrightness = sharedPreferences.getInt("arduino_brightness", 100);
        arduino = new Arduino(arduinoName, arudinoIP, arduinoPower ? Arduino.Power.On : Arduino.Power.Standby,
                arduinoBrightness, String.format("#%06X", 0xFFFFFF & arduinoColor));
        String mhzName = sharedPreferences.getString("mhz_name", "default");
        String mhzIP = sharedPreferences.getString("mhz_ip", "192.168.178.1");
        String mhzNet = sharedPreferences.getString("mhz_net", "11111");
        String mhzDevice = sharedPreferences.getString("mhz_device", "10000");
        mhz = new Mhz(mhzName, mhzNet, mhzDevice, mhzIP);
        colourPresets = new int[19];
        for (int i = 1; i < 20; i++) {
            colourPresets[i - 1] = sharedPreferences.getInt("color" + (i < 10 ? "0" : "") + i,
                    activity.getResources().getIntArray(R.array.presets)[i - 1]);
        }
    }

    /**
     * Returns the refreshrate
     * @return refreshrate
     */
    public int getRefreshrate() {
        return refreshrate;
    }

    /**
     * Returns the colorInt used for the application color
     * @return colour
     */
    public int getColour() {
        return colour;
    }

    /**
     * Returns the default Arduino
     * @return arduino
     */
    public Arduino getArduino() {
        return arduino;
    }

    /**
     * Returns the default Mhz device
     * @return mhz
     */
    public Mhz getMhz() {
        return mhz;
    }

    /**
     * Returns the colorPresets Array
     * @return colourPresets
     */
    public int[] getColourPresets() {
        return colourPresets;
    }

    /**
     * sets the refreshrate, checks for a valid value (1,5,10,30,60,120,300)
     * @param refreshrate new refreshrate
     */
    public void setRefreshrate(int refreshrate) {
        if (refreshrate == 1 || refreshrate == 5 || refreshrate == 10 || refreshrate == 15
                || refreshrate == 30 || refreshrate == 60 || refreshrate == 120 || refreshrate == 300)
            this.refreshrate = refreshrate;
    }

    /**
     * sets the application color
     * @param colour new color Int
     */
    public void setColour(@ColorInt int colour) {
        this.colour = colour;
    }

    /**
     * sets the default arduino
     * @param arduino new arduino
     */
    private void setArduino(Arduino arduino) {
        this.arduino = arduino;
    }

    /**
     * sets the default mhz
     * @param mhz new mhz
     */
    private void setMhz(Mhz mhz) {
        this.mhz = mhz;
    }

    /**
     * sets the colorPreset at the given position
     * @param colourPreset new color int
     * @param pos position to update
     */
    private void setColourPreset(@ColorInt int colourPreset, int pos) {
        if (pos >= 0 && pos < 20)
            this.colourPresets[pos] = colourPreset;
    }

    /**
     * TODO: replace switch with substring --
     * Updates the corresponding color to the given key
     * @param key key to update
     * @param newValue new value
     */
    public void updatePreferenceColor(String key, Object newValue) {
        switch (key) {
            case "color01": {
                setColourPreset((int) newValue, 0);
                break;
            }
            case "color02": {
                setColourPreset((int) newValue, 1);
                break;
            }
            case "color03": {
                setColourPreset((int) newValue, 2);
                break;
            }
            case "color04": {
                setColourPreset((int) newValue, 3);
                break;
            }
            case "color05": {
                setColourPreset((int) newValue, 4);
                break;
            }
            case "color06": {
                setColourPreset((int) newValue, 5);
                break;
            }
            case "color07": {
                setColourPreset((int) newValue, 6);
                break;
            }
            case "color08": {
                setColourPreset((int) newValue, 7);
                break;
            }
            case "color09": {
                setColourPreset((int) newValue, 8);
                break;
            }
            case "color10": {
                setColourPreset((int) newValue, 9);
                break;
            }
            case "color11": {
                setColourPreset((int) newValue, 10);
                break;
            }
            case "color12": {
                setColourPreset((int) newValue, 11);
                break;
            }
            case "color13": {
                setColourPreset((int) newValue, 12);
                break;
            }
            case "color14": {
                setColourPreset((int) newValue, 13);
                break;
            }
            case "color15": {
                setColourPreset((int) newValue, 14);
                break;
            }
            case "color16": {
                setColourPreset((int) newValue, 15);
                break;
            }
            case "color17": {
                setColourPreset((int) newValue, 16);
                break;
            }
            case "color18": {
                setColourPreset((int) newValue, 17);
                break;
            }
            case "color19": {
                setColourPreset((int) newValue, 18);
                break;
            }
        }
    }

    /**
     * Updates the corresponding value to the key (not colorPresets)
     * @param key key to update
     * @param newValue new value
     */
    public void updatePreference(String key, Object newValue) {
        switch (key) {
            case "arduino_name": {
                Arduino tmp = getArduino();
                tmp.name = (String) newValue;
                setArduino(tmp);
                break;
            }
            case "arduino_ip": {
                Arduino tmp = getArduino();
                tmp.ip = (String) newValue;
                setArduino(tmp);
                break;
            }
            case "arduino_color": {
                Arduino tmp = getArduino();
                tmp.colour = String.format("#%06X", 0xFFFFFF & (int) newValue);
                setArduino(tmp);
                break;
            }
            case "arduino_power": {
                Arduino tmp = getArduino();
                tmp.power = (boolean) newValue ? Arduino.Power.On : Arduino.Power.Standby;
                setArduino(tmp);
                break;
            }
            case "arduino_brightness": {
                Arduino tmp = getArduino();
                tmp.brightness = (int) newValue;
                setArduino(tmp);
                break;
            }
            case "mhz_name": {
                Mhz tmp = getMhz();
                tmp.name = (String) newValue;
                setMhz(tmp);
                break;
            }
            case "mhz_ip": {
                Mhz tmp = getMhz();
                tmp.ip = (String) newValue;
                setMhz(tmp);
                break;
            }
            case "mhz_net": {
                Mhz tmp = getMhz();
                tmp.net = (String) newValue;
                setMhz(tmp);
                break;
            }
            case "mhz_device": {
                Mhz tmp = getMhz();
                tmp.device = (String) newValue;
                setMhz(tmp);
                break;
            }
            case "mhz_power": {
                Mhz tmp = getMhz();
                tmp.state = (boolean) newValue;
                setMhz(tmp);
                break;
            }
        }
    }
}
