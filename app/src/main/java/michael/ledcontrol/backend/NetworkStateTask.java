/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import android.support.v4.widget.SwipeRefreshLayout;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * Contains a ScheduledExecutorService for pinging the Arduinos and Mhz-Device servers and updating the View accordingly
 *
 * @author Michael Schmitz
 */
public class NetworkStateTask {
    private static NetworkStateTask instance;
    private ScheduledExecutorService exec;
    private Runnable pingRunnable;
    private ScheduledFuture<?> handle;

    /**
     * Constructor
     * Task based on a ScheduledExecutorService,
     * executed according to the given delay in seconds
     * pings all Arduinos and sets the PowerIcon accordingly
     * pings all Mhz device Servers and sets the server state accordingly
     * querys all Mhz device states at the server and sets the state accordingly
     *
     * @param activity    MainActivity needed for updating the view states
     * @param refreshrate Delay for the execution
     */
    private NetworkStateTask(final MainActivity activity, int refreshrate) {
        exec = Executors.newScheduledThreadPool(1);
        pingRunnable = new Runnable() {
            @Override
            public void run() {
                List<Network> networks = ArduinoDataModel.getInstance(activity).getNetworks();
                List<Mhz> mhzs = MhzDataModel.getInstance(activity).getMhzs();
                Map<String, Network> networkMap = MhzDataModel.getInstance(activity).getNetworks();
                if (networks != null && networks.size() != 0) {
                    for (int i = 0; i < networks.size(); i++) {
                        updateNetworkingPower(networks.get(i), i, activity);
                    }
                }
                if (networkMap != null && networkMap.size() != 0) {
                    Set<String> keys = networkMap.keySet();
                    for (String key : keys) {
                        updateServerState(networkMap.get(key), key, activity);
                    }
                    for (Mhz mhz : mhzs) {
                        queryDeviceState(mhz, networkMap.get(mhz.ip));
                    }
                }
            }
        };

        handle = exec.scheduleWithFixedDelay(pingRunnable, 5, refreshrate, TimeUnit.SECONDS);
    }

    /**
     * Creates an instance of NetworkStateTask if none exists, returns the instance
     *
     * @param activity needed to instantiate NetworkStateTask
     * @param refreshrate needed to instantiate NetworkStateTask
     * @return instance of NetworkStateTask
     */
    public static synchronized NetworkStateTask getInstance(MainActivity activity, int refreshrate) {
        if (instance == null)
            instance = new NetworkStateTask(activity, refreshrate);
        return instance;
    }

    /**
     * Uses the same functionality as the TaskInstance but is only executed once
     *
     * @param activity needed to update the view states
     * @param refreshLayout needed to set the refresh finished when done
     * @param arduino boolean that indicates if currently the Arduino or Mhz RecyclerView is upfront
     */
    public void pingOnce(final MainActivity activity, final SwipeRefreshLayout refreshLayout, boolean arduino) {
        Executor execOnce = Executors.newSingleThreadExecutor();
        Runnable pingOnce;
        if (arduino) {
            pingOnce = new Runnable() {
                @Override
                public void run() {
                    List<Network> networks = ArduinoDataModel.getInstance(activity).getNetworks();
                    if (networks != null && networks.size() != 0) {
                        for (int i = 0; i < networks.size(); i++) {
                            updateNetworkingPower(networks.get(i), i, activity);
                        }
                    }
                    if (refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                }
            };


        } else {
            pingOnce = new Runnable() {
                @Override
                public void run() {
                    Map<String, Network> networkMap = MhzDataModel.getInstance(activity).getNetworks();
                    List<Mhz> mhzs = MhzDataModel.getInstance(activity).getMhzs();
                    if (networkMap != null && networkMap.size() != 0) {
                        Set<String> keys = networkMap.keySet();
                        for (String key : keys) {
                            updateServerState(networkMap.get(key), key, activity);
                        }
                        for (Mhz mhz : mhzs) {
                            queryDeviceState(mhz, networkMap.get(mhz.ip));
                        }
                    }
                    if (refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                }
            };
        }
        execOnce.execute(pingOnce);
    }

    /**
     * Cancels the current task, when canceled shuts down the executor creates a new one and restarts the task with the new refreshrate
     *
     * @param refreshrate new refreshrate
     */
    public void restartTask(int refreshrate) {
        handle.cancel(true);

        if (handle.isCancelled()) {
            exec.shutdown();
            exec = Executors.newScheduledThreadPool(1);
            handle = exec.scheduleWithFixedDelay(pingRunnable, 0, refreshrate, TimeUnit.SECONDS);
        }
    }

    /**
     * Helper method to send a message to the server and query the state of the Mhz device
     *
     * @param mhz Mhz device to query
     * @param network Network instance to send the message
     */
    private void queryDeviceState(Mhz mhz, Network network) {
        int[] msg = new int[6];
        int sum = 0;
        msg[0] = 0xCC;
        msg[1] = 0xDD;
        msg[2] = Integer.parseInt(mhz.net, 2);
        msg[3] = Integer.parseInt(mhz.device, 2);
        for (int i = 0; i < 6; i++) {
            sum += msg[i];
        }
        msg[4] = sum >> 8;
        msg[5] = sum & 0x00FF;
        network.sendMessage(msg);
    }

    /**
     * Updates the server state of Mhz devices, is run on the UI Thread
     *
     * @param network Network instance of the server
     * @param ip IP that's updated
     * @param activity activity to run the ViewUpdate on
     */
    private void updateServerState(Network network, final String ip, final MainActivity activity) {
        if (ping(network.getLocal()))
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MhzDataModel.getInstance(activity).updateServerState(ip, true);
                }
            });
        else
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MhzDataModel.getInstance(activity).updateServerState(ip, false);
                }
            });
    }

    /**
     * Helper method for NetworkStateTask method
     * Pings and sets the power information in ArduinoDataModel accordingly
     *
     * @param network Instance of Network of the current Arduino
     * @param i       Position of Arduino in the ListView
     */
    private void updateNetworkingPower(Network network, final int i, final MainActivity activity) {
        if (ping(network.getLocal())) {
            switch (ArduinoDataModel.getInstance(activity).getArduinos().get(i).power) {
                case noPowerStandby: {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.Standby, i);
                        }
                    });
                    break;
                }
                case noPowerOn: {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.On, i);
                        }
                    });
                    break;
                }
            }
        } else {
            switch (ArduinoDataModel.getInstance(activity).getArduinos().get(i).power) {
                case Standby: {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.noPowerStandby, i);
                        }
                    });
                    break;
                }
                case On: {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.noPowerOn, i);
                        }
                    });
                    break;
                }
            }
        }

    }

    /**
     * Tests if the network device is reachable
     *
     * @return true, if network device is reachable; false, if not or an Exception occurred
     */
    private boolean ping(InetAddress local) {
        try {
            return local.isReachable(500);
        } catch (IOException e) {
            return false;
        }
    }
}
