/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.backend;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import michael.ledcontrol.ui.main.MainActivity;

/**
 * UDPListener to wait for the response on the querys made for the Mhz device state
 *
 * @author Michael Schmitz
 */
public class UDPListener {
    private static UDPListener instance;

    /**
     * Constructor
     * loads the socket which made the queries and waits for packets, if one is recieved it is parsed
     * and then the device accordingly updated
     * @param activity needed to update the mhz devices view
     */

    private UDPListener(final MainActivity activity) {

        ExecutorService exec = Executors.newSingleThreadExecutor();
        Runnable listener = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        DatagramSocket udpSocket = Network.getSocket();
                        if (udpSocket != null) {
                            byte[] message = new byte[6];
                            DatagramPacket packet = new DatagramPacket(message, message.length);
                            udpSocket.receive(packet);
                            int device = message[3];
                            int net = message[2];
                            boolean power = message[1] == 49;
                            updatePowerMhz(net, device, power, activity);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        exec.execute(listener);
    }

    /**
     * Creates an instance of UDPListener if none exists, returns the instance
     * @param activity needed to instantiate UDPListener
     * @return Instance of UDPListener
     */
    public static synchronized UDPListener getInstance(MainActivity activity) {
        if (instance == null)
            instance = new UDPListener(activity);
        return instance;
    }

    /**
     *
     * @param net net identifier
     * @param device device identifier
     * @param power power state of device
     * @param activity activity to run the view update on the UI Thread
     */
    private void updatePowerMhz(final int net, final int device, final boolean power, final MainActivity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int pos = MhzDataModel.getInstance(activity).getPos(net, device);
                MhzDataModel.getInstance(activity).setPower(power, pos);
            }
        });
    }

}
