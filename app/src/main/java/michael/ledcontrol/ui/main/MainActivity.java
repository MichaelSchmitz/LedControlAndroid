/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package michael.ledcontrol.ui.main;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.jaredrummler.android.colorpicker.ColorShape;

import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.ColorConverter;
import michael.ledcontrol.backend.ColorPresets;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.MhzDataModel;
import michael.ledcontrol.backend.NetworkStateTask;
import michael.ledcontrol.backend.SettingsDataModel;
import michael.ledcontrol.backend.UDPListener;
import michael.ledcontrol.ui.arduino.ArduinoBrightnessDialogFragment;
import michael.ledcontrol.ui.arduino.ArduinoEditDialogFragment;
import michael.ledcontrol.ui.arduino.ArduinoFragment;
import michael.ledcontrol.ui.mhz.MhzEditDialogFragment;
import michael.ledcontrol.ui.mhz.MhzFragment;
import michael.ledcontrol.ui.settings.SettingsFragment;

/**
 * Main entrypoint for application, manages the fragments, FAB, and NavigationBar
 *
 * @author Michael Schmitz
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, ColorPickerDialogListener {
    private BottomNavigationView bottomNavigationView;
    public FAB fabs;

    /**
     * sets saved Settings like color, initializes UDPListener, FABs and
     * creates the BottomNavBar with its listener, loads the previously opened fragment
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        int color = SettingsDataModel.getInstance(this).getColour();
        setStatusbar(color);
        bottomNavigationView = findViewById(R.id.navigation);
        setBottomNavColor(color);
        fabs = new FAB(color, this, bottomNavigationView);
        NetworkStateTask.getInstance(this, SettingsDataModel.getInstance(this).getRefreshrate());
        UDPListener.getInstance(this);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_arduino:
                                fabs.showFab();
                                if (fabs.getFABState())
                                    fabs.animateFAB();
                                selectedFragment = new ArduinoFragment();
                                break;
                            case R.id.action_mhz:
                                fabs.showFab();
                                if (fabs.getFABState())
                                    fabs.animateFAB();
                                selectedFragment = new MhzFragment();
                                break;
                            case R.id.action_settings:
                                if (fabs.getFABState())
                                    fabs.animateFAB();
                                fabs.hideFab();
                                selectedFragment = new SettingsFragment();
                                break;
                        }
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (savedInstanceState != null) {
            bottomNavigationView.setSelectedItemId(savedInstanceState.getInt("FragmentId"));
            switch (bottomNavigationView.getSelectedItemId()) {
                case R.id.action_arduino:
                    transaction.replace(R.id.frame, new ArduinoFragment());
                    break;
                case R.id.action_mhz:
                    transaction.replace(R.id.frame, new MhzFragment());
                    break;
                case R.id.action_settings:
                    transaction.replace(R.id.frame, new SettingsFragment());
                    break;
            }
            if (savedInstanceState.getBoolean("FABOpen"))
                fabs.animateFAB();
        } else
            transaction.replace(R.id.frame, new ArduinoFragment());
        transaction.commit();
    }

    /**
     * Saves the current state of the FAB and the currently selected fragment
     * @param outState Bundle outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("FABOpen", fabs.getFABState());
        outState.putInt("FragmentId", bottomNavigationView.getSelectedItemId());
    }

    /**
     * sets the given color to the FAB, statusbar and NavigationBar
     * @param color colorInt
     */
    public void setColor(int color) {
        fabs.setFABColor(color);
        setStatusbar(color);
        setBottomNavColor(color);
    }

    /**
     * sets the given color to the NavigationBar (text and icons)
     * @param color colorInt
     */
    private void setBottomNavColor(int color) {
        ColorStateList colorStateList = new ColorStateList(new int[][]{new int[]{android.R.attr.state_checked},
                new int[]{-android.R.attr.state_checked}}, new int[]{color, 0x89000000});
        bottomNavigationView.setItemIconTintList(colorStateList);
        bottomNavigationView.setItemTextColor(colorStateList);
    }


    /**
     * sets the given color to the status and toolbar, changes the application titel and all icons
     * of the application as well as statusbar icons, if the color is to bright to see
     * the white icons anymore (and back, if its not the case)
     *
     * @param color colorInt
     */
    private void setStatusbar(int color) {
        Window w = getWindow();
        View view = w.getDecorView();
        int flags = view.getSystemUiVisibility();
        if (ColorConverter.getBrightness(String.format("#%06X", 0xFFFFFF & color)) < 0.4) {
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        } else {
            flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        }
        view.setSystemUiVisibility(flags);
        w.setStatusBarColor(color);
    }

    /**
     * Listener for FAB
     * differs between ArduinoFragment and MhzFragment
     * triggers the corresponding actions
     * @param v View
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (bottomNavigationView.getSelectedItemId()) {
            case R.id.action_arduino:
                fabs.animateFAB();
                switch (id) {
                    case R.id.miniFabAdd:
                        ArduinoEditDialogFragment.arduinoEdit(true, getFragmentManager());
                        break;
                    case R.id.miniFabDeviceSettings:
                        ArduinoEditDialogFragment.arduinoEdit(false, getFragmentManager());
                        break;
                    case R.id.miniFabSelectAll:
                        for (int i = 0; i < ArduinoDataModel.getInstance(this).getArduinos().size(); i++) {
                            ArduinoDataModel.getInstance(this).setChecked(true, i);
                        }
                        break;
                    case R.id.miniFabDeselectAll:
                        for (int i = 0; i < ArduinoDataModel.getInstance(this).getArduinos().size(); i++) {
                            ArduinoDataModel.getInstance(this).setChecked(false, i);
                        }
                        break;
                    case R.id.miniFabPower:
                        List<Integer> checked = ArduinoDataModel.getInstance(this).whichAreChecked();
                        for (int i : checked) {
                            if (ArduinoDataModel.getInstance(this).getArduinos().get(i).power == Arduino.Power.On)
                                ArduinoDataModel.getInstance(this).setPower(Arduino.Power.Standby, i);
                            else if (ArduinoDataModel.getInstance(this).getArduinos().get(i).power == Arduino.Power.Standby)
                                ArduinoDataModel.getInstance(this).setPower(Arduino.Power.On, i);
                            else
                                Snackbar.makeSnackbar(R.string.error_completly_off, this);
                            ArduinoDataModel.getInstance(this).setChecked(false, i);
                        }

                        break;
                    case R.id.miniFabBrightness:
                        ArduinoBrightnessDialogFragment arduinoBrightnessDialogFragment = new ArduinoBrightnessDialogFragment();
                        arduinoBrightnessDialogFragment.show(getFragmentManager(), "brightness");
                        break;
                    case R.id.miniFabColor:
                        String col = ArduinoDataModel.getInstance(this).getArduinos().get(ArduinoDataModel.getInstance(this).whichAreChecked().get(0)).colour;
                        int color = Color.BLACK;
                        if (col.matches("#[0-9a-fA-F]{6}"))
                            color = Color.parseColor(col);
                        ColorPickerDialog.newBuilder().setDialogType(ColorPickerDialog.TYPE_PRESETS)
                                .setColorShape(ColorShape.CIRCLE)
                                .setAllowCustom(true)
                                .setAllowPresets(true)
                                .setShowModes(true)
                                .setColor(color)
                                .setPresets(ColorPresets.getInstance(this).getPresets())
                                .setDialogId(2).show(this);

                        break;
                    case R.id.miniFabRefresh:
                        NetworkStateTask.getInstance(this, SettingsDataModel.getInstance(this).getRefreshrate())
                                .pingOnce(this, null, true);
                        break;
                }
                break;
            case R.id.action_mhz:
                fabs.animateFAB();
                switch (id) {
                    case R.id.miniFabAdd:
                        MhzEditDialogFragment.mhzEdit(true, getFragmentManager());
                        break;
                    case R.id.miniFabDeviceSettings:
                        MhzEditDialogFragment.mhzEdit(false, getFragmentManager());
                        break;
                    case R.id.miniFabSelectAll:
                        for (int i = 0; i < MhzDataModel.getInstance(this).getMhzs().size(); i++) {
                            MhzDataModel.getInstance(this).setChecked(true, i);
                        }
                        break;
                    case R.id.miniFabDeselectAll:
                        for (int i = 0; i < MhzDataModel.getInstance(this).getMhzs().size(); i++) {
                            MhzDataModel.getInstance(this).setChecked(false, i);
                        }
                        break;
                    case R.id.miniFabPower:
                        List<Integer> checked = MhzDataModel.getInstance(this).whichAreChecked();
                        for (int i : checked) {
                            if (MhzDataModel.getInstance(this).getMhzs().get(i).serverUp)
                                MhzDataModel.getInstance(this).setPower(!MhzDataModel.getInstance(this).getMhzs().get(i).state, i);
                            else
                                Snackbar.makeSnackbar(R.string.serverDown, this);
                            MhzDataModel.getInstance(this).setChecked(false, i);
                        }
                        break;
                    case R.id.miniFabRefresh:
                        NetworkStateTask.getInstance(this, SettingsDataModel.getInstance(this).getRefreshrate())
                                .pingOnce(this, null, false);
                        break;
                }
                break;
        }
    }

    /**
     * Listener for the ColorDialogs, sets either the fragments TextView Color or for the selected Arduinos
     * or the color of the status and toolbar
     */
    @Override
    public void onColorSelected(int dialogId, int color) {
        if (dialogId == 1) {
            ArduinoEditDialogFragment fragment = (ArduinoEditDialogFragment) getFragmentManager()
                    .findFragmentByTag("arduinoEdit");
            switch (color) {
                case 0xAA000000: {
                    fragment.updatePopupColour(R.string.rainbow, "", "rainbow", Color.BLACK);
                    break;
                }
                case 0xBB000000: {
                    fragment.updatePopupColour(R.string.rainbow_circle, "", "rainbow_circle", Color.BLACK);
                    break;
                }
                case 0xCC000000: {
                    fragment.updatePopupColour(R.string.rainbow_sinus, "", "rainbow_sinus", Color.BLACK);
                    break;
                }
                case 0xDD000000: {
                    fragment.updatePopupColour(R.string.spectrum, "", "spectrum", Color.BLACK);
                    break;
                }
                case 0xEE000000: {
                    fragment.updatePopupColour(R.string.spectrum_sinus, "", "spectrum_sinus", Color.BLACK);
                    break;
                }
                default: {
                    fragment.updatePopupColour(0, ColorConverter.getNameByHex("#" + Integer.toHexString(color).substring(2)),
                            "#" + Integer.toHexString(color).substring(2), color);
                }
            }
        } else if (dialogId == 2) {
            final List<Integer> selected = ArduinoDataModel.getInstance(this).whichAreChecked();
            for (int i : selected) {
                ArduinoDataModel.getInstance(this).setChecked(false, i);
                switch (color) {
                    case 0xAA000000: {
                        ArduinoDataModel.getInstance(this).setColour("rainbow", i);
                        break;
                    }
                    case 0xBB000000: {
                        ArduinoDataModel.getInstance(this).setColour("rainbow_circle", i);
                        break;
                    }
                    case 0xCC000000: {
                        ArduinoDataModel.getInstance(this).setColour("rainbow_sinus", i);
                        break;
                    }
                    case 0xDD000000: {
                        ArduinoDataModel.getInstance(this).setColour("spectrum", i);
                        break;
                    }
                    case 0xEE000000: {
                        ArduinoDataModel.getInstance(this).setColour("spectrum_sinus", i);
                        break;
                    }
                    default: {
                        ArduinoDataModel.getInstance(this).setColour("#" + Integer.toHexString(color).substring(2), i);
                    }
                }

            }
        } else if (dialogId >= 3) {
            int rowID = dialogId - 3;
            if (rowID >= ArduinoDataModel.getInstance(this).getArduinos().size())
                return;
            switch (color) {
                case 0xAA000000: {
                    ArduinoDataModel.getInstance(this).setColour("rainbow", rowID);
                    break;
                }
                case 0xBB000000: {
                    ArduinoDataModel.getInstance(this).setColour("rainbow_circle", rowID);
                    break;
                }
                case 0xCC000000: {
                    ArduinoDataModel.getInstance(this).setColour("rainbow_sinus", rowID);
                    break;
                }
                case 0xDD000000: {
                    ArduinoDataModel.getInstance(this).setColour("spectrum", rowID);
                    break;
                }
                case 0xEE000000: {
                    ArduinoDataModel.getInstance(this).setColour("spectrum_sinus", rowID);
                    break;
                }
                default: {
                    ArduinoDataModel.getInstance(this).setColour("#" + Integer.toHexString(color).substring(2), rowID);
                }
            }
        }
    }

    /**
     * not needed, but interface requires
     * @param dialogId dialogId
     */
    @Override
    public void onDialogDismissed(int dialogId) {
    }
}
