/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.main;

import android.view.View;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.backend.MhzDataModel;

/**
 * Class for creating Snackbars and displaying them above the bottom button row
 *
 * @author Michael Schmitz
 */
public class Snackbar {
    /**
     * Creates a Snackbar with the given Message and an Undo Button
     *
     * @param resText RessourceId of the text to be displayed
     * @param arduino Arduino Object, to be inserted again if User clicks Undo
     * @param pos     Position of the arduino Object before deletion
     */
    public static void makeSnackbar(int resText, final MainActivity application, final Arduino arduino, final int pos) {
        android.support.design.widget.Snackbar snackbar = android.support.design.widget.Snackbar.make(application.findViewById(R.id.coordinatorLayout), resText, android.support.design.widget.Snackbar.LENGTH_SHORT);
        snackbar.setAction("Undo", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArduinoDataModel.getInstance(application).addAtPos(arduino, pos);
            }
        }).show();
    }

    /**
     * Creates a Snackbar with the given Message and an Undo Button
     *
     * @param resText RessourceId of the text to be displayed
     * @param mhz     Mhz Object, to be inserted again if User clicks Undo
     * @param pos     Position of the arduino Object before deletion
     */
    public static void makeSnackbar(int resText, final MainActivity application, final Mhz mhz, final int pos) {
        android.support.design.widget.Snackbar snackbar = android.support.design.widget.Snackbar.make(application.findViewById(R.id.coordinatorLayout), resText, android.support.design.widget.Snackbar.LENGTH_SHORT);
        snackbar.setAction("Undo", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MhzDataModel.getInstance(application).addAtPos(mhz, pos);
            }
        }).show();
    }

    /**
     * Creates a Snackbar with the given text
     *
     * @param resText RessourceId of the text to be displayed
     */
    public static void makeSnackbar(int resText, MainActivity application) {
        android.support.design.widget.Snackbar.make(application.findViewById(R.id.coordinatorLayout), resText, android.support.design.widget.Snackbar.LENGTH_SHORT).show();
    }

    /**
     * Creates a Snackbar with the given text
     *
     * @param text CharSequence to be displayed
     */
    public static void makeSnackbar(CharSequence text, MainActivity application) {
        android.support.design.widget.Snackbar.make(application.findViewById(R.id.coordinatorLayout), text, android.support.design.widget.Snackbar.LENGTH_SHORT).show();
    }

}
