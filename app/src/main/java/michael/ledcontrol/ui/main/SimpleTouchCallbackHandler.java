/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.main;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.ui.arduino.ArduinoRecyclerView;
import michael.ledcontrol.ui.mhz.MhzRecyclerView;

/**
 * Touch Handler, needed for Swipe detection as well as Drag'n'Drop feature
 *
 * @author Michael Schmitz
 */
public class SimpleTouchCallbackHandler extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    public SimpleTouchCallbackHandler(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    /**
     * Calls implementation in the listener
     *
     * @param recyclerView current RecyclerView
     * @param viewHolder   ViewHolder of dragged item
     * @param target       ViewHolder of item that's dragged onto
     * @return always true
     */
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        listener.onMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    /**
     * Default implementation, but only for the foreground (not background)
     * @param viewHolder selected ViewHolder
     * @param actionState action that was performed
     */
    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            final View foregroundView = ((ViewHolderInterface) viewHolder).getForeground();
            getDefaultUIUtil().onSelected(foregroundView);
        }
    }

    /**
     * If action is swipe the foreground is moved so the background is visible
     * for every other action: default implementation
     * @param c Canvas which is used to draw changes
     * @param recyclerView current RecyclerView
     * @param viewHolder selected ViewHolder
     * @param dX current x-Position
     * @param dY current y-Position
     * @param actionState gesture
     * @param isCurrentlyActive boolean
     */
    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            final View foregroundView = ((ViewHolderInterface) viewHolder).getForeground();
            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive);
        } else
            super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    /**
     * Default implementation with one addition: the database gets reordered based on the view
     * @param recyclerView current RecyclerView
     * @param viewHolder viewHolder that was used
     */
    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        View foregroundView = ((ViewHolderInterface) viewHolder).getForeground();
        getDefaultUIUtil().clearView(foregroundView);
        if (recyclerView.getAdapter() instanceof ArduinoRecyclerView)
            ArduinoDataModel.getInstance(((ArduinoRecyclerView) recyclerView.getAdapter()).getActivity()).reorderDatabase();
        else if (recyclerView.getAdapter() instanceof MhzRecyclerView)
            ArduinoDataModel.getInstance(((MhzRecyclerView) recyclerView.getAdapter()).getActivity()).reorderDatabase();
    }

    /**
     *
     * Default implementation
     * @param c Canvas which is used to draw changes
     * @param recyclerView current RecyclerView
     * @param viewHolder selected ViewHolder
     * @param dX current x-Position
     * @param dY current y-Position
     * @param actionState gesture
     * @param isCurrentlyActive boolean
     */
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            final View foregroundView = ((ViewHolderInterface) viewHolder).getForeground();

            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                    actionState, isCurrentlyActive);
        } else
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    /**
     * Calls the method in the listener class
     * @param viewHolder ViewHolder that is swiped
     * @param direction direction of swipe
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    /**
     * Defines the methods to be implemented in the listener class
     */
    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);

        void onMove(int fromPos, int toPos);
    }
}
