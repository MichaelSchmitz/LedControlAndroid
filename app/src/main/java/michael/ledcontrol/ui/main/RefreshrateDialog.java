/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.main;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.NetworkStateTask;
import michael.ledcontrol.backend.SettingsDataModel;

/**
 * TODO: modularize
 * DialogFragment which inflates the application_slider.xml layout
 * Use this to open a new AlertView with a RefreshrateSlider
 *
 * @author Michael Schmitz
 */
public class RefreshrateDialog {
    public static Dialog getDialog(final MenuItem item, final MainActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alert = inflater.inflate(R.layout.application_slider, null);
        final TextView textView = alert.findViewById(R.id.seekBarPercentage);
        final SeekBar seekBar = alert.findViewById(R.id.seekBar);
        //set the startValues to current Values (of first or only selected element)
        int value = SettingsDataModel.getInstance(activity).getRefreshrate();
        seekBar.setProgress(value);
        textView.setText(value);
        builder.setView(alert);
        //tracks the userMovement on the slider and sets the TextView accordingly
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                textView.setText(String.valueOf(progress));
            }
        });
        builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int refreshrateOld = SettingsDataModel.getInstance(activity).getRefreshrate();
                SettingsDataModel.getInstance(activity).setRefreshrate(seekBar.getProgress());
                item.setTitle(String.format(activity.getString(R.string.refreshrate), SettingsDataModel.getInstance(activity).getRefreshrate()));
                NetworkStateTask.getInstance(activity, refreshrateOld).restartTask(SettingsDataModel.getInstance(activity).getRefreshrate());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

}
