package michael.ledcontrol.ui.main;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.ColorConverter;
import michael.ledcontrol.backend.MhzDataModel;

/**
 * Created by michael on 17.03.18.
 */
public class FAB {
    private CoordinatorLayout.LayoutParams refresh;
    private Boolean isFabOpen;
    private FloatingActionButton fab, miniFabSelectAll, miniFabDeselect, miniFabAdd, miniFabSetting,
            miniFabPower, miniFabBrightness, miniFabColor, miniFabRefresh;
    private Animation fab_open, fab_open2, fab_open3, fab_open4, fab_open5, fab_open6, fab_close,
            fab_close2, fab_close3, fab_close4, fab_close5, fab_close6, fab_close_main,
            fab_open_main, rotate_forward, rotate_backward;
    private MainActivity activity;
    private BottomNavigationView bottomNav;

    public boolean getFABState() {
        return isFabOpen;
    }

    protected FAB(int color, MainActivity activity, BottomNavigationView bottomNav) {
        Context appContext = activity.getApplicationContext();
        isFabOpen = false;
        this.activity = activity;
        this.bottomNav = bottomNav;
        fab = activity.findViewById(R.id.fab);
        miniFabAdd = activity.findViewById(R.id.miniFabAdd);
        miniFabSetting = activity.findViewById(R.id.miniFabDeviceSettings);
        miniFabSelectAll = activity.findViewById(R.id.miniFabSelectAll);
        miniFabDeselect = activity.findViewById(R.id.miniFabDeselectAll);
        miniFabBrightness = activity.findViewById(R.id.miniFabBrightness);
        miniFabColor = activity.findViewById(R.id.miniFabColor);
        miniFabPower = activity.findViewById(R.id.miniFabPower);
        miniFabRefresh = activity.findViewById(R.id.miniFabRefresh);

        refresh = (CoordinatorLayout.LayoutParams) miniFabRefresh.getLayoutParams();
        fab_close_main = AnimationUtils.loadAnimation(appContext, R.anim.fab_close_main);
        fab_open_main = AnimationUtils.loadAnimation(appContext, R.anim.fab_open_main);


        fab_open = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open2 = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open3 = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open4 = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open5 = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open6 = AnimationUtils.loadAnimation(appContext, R.anim.fab_open);
        fab_open2.setStartOffset(50);
        fab_open3.setStartOffset(100);
        fab_open4.setStartOffset(150);
        fab_open5.setStartOffset(200);
        fab_open6.setStartOffset(250);

        fab_close = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close2 = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close3 = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close4 = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close5 = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close6 = AnimationUtils.loadAnimation(appContext, R.anim.fab_close);
        fab_close.setStartOffset(250);
        fab_close2.setStartOffset(200);
        fab_close3.setStartOffset(150);
        fab_close4.setStartOffset(100);
        fab_close5.setStartOffset(50);

        rotate_forward = AnimationUtils.loadAnimation(appContext, R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(appContext, R.anim.rotate_backward);
        fab.setOnClickListener(activity);
        miniFabAdd.setOnClickListener(activity);
        miniFabSetting.setOnClickListener(activity);
        miniFabSelectAll.setOnClickListener(activity);
        miniFabDeselect.setOnClickListener(activity);
        miniFabBrightness.setOnClickListener(activity);
        miniFabColor.setOnClickListener(activity);
        miniFabPower.setOnClickListener(activity);
        miniFabRefresh.setOnClickListener(activity);
        setFABColor(color);
    }

    protected void setFABColor(int color) {
        fab.setBackgroundTintList(ColorStateList.valueOf(color));
        if (ColorConverter.getBrightness(String.format("#%06X", 0xFFFFFF & color)) < 0.4) {
            fab.getDrawable().setTint(0x89000000);
        } else {
            fab.getDrawable().setTint(0xFFFFFFFF);
        }
    }

    public void animateFAB() {
        if (bottomNav.getSelectedItemId() == R.id.action_arduino) {
            if (isFabOpen) {
                fab.startAnimation(rotate_backward);
                if (ArduinoDataModel.getInstance(activity).whichAreChecked().size() > 0) {
                    miniFabRefresh.startAnimation(fab_close6);
                    miniFabColor.startAnimation(fab_close5);
                    miniFabBrightness.startAnimation(fab_close4);
                    miniFabPower.startAnimation(fab_close3);
                    miniFabSetting.startAnimation(fab_close2);
                    miniFabAdd.clearAnimation();
                    miniFabRefresh.setClickable(false);
                    miniFabColor.setClickable(false);
                    miniFabBrightness.setClickable(false);
                    miniFabPower.setClickable(false);
                    miniFabSetting.setClickable(false);
                    if (ArduinoDataModel.getInstance(activity).whichAreChecked().size() == ArduinoDataModel.getInstance(activity).getArduinos().size()) {
                        miniFabSelectAll.clearAnimation();
                        miniFabDeselect.startAnimation(fab_close);
                        miniFabDeselect.setClickable(false);
                    } else {
                        miniFabDeselect.clearAnimation();
                        miniFabSelectAll.startAnimation(fab_close);
                        miniFabSelectAll.setClickable(false);
                    }
                } else {
                    miniFabSetting.clearAnimation();
                    miniFabPower.clearAnimation();
                    miniFabBrightness.clearAnimation();
                    miniFabColor.clearAnimation();

                    miniFabRefresh.startAnimation(fab_close6);
                    miniFabAdd.startAnimation(fab_close5);
                    miniFabSelectAll.startAnimation(fab_close4);
                    miniFabRefresh.setClickable(false);
                    miniFabAdd.setClickable(false);
                    miniFabSelectAll.setClickable(false);
                }
                isFabOpen = false;
            } else {
                fab.startAnimation(rotate_forward);
                int orientation = activity.getResources().getConfiguration().orientation;
                if (ArduinoDataModel.getInstance(activity).whichAreChecked().size() > 0) {
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        refresh.setMargins(0, 0, getPixelFromDP(86), getPixelFromDP(230));
                    else
                        refresh.setMargins(0, 0, getPixelFromDP(16), getPixelFromDP(440));
                    miniFabRefresh.setLayoutParams(refresh);
                    miniFabRefresh.startAnimation(fab_open6);
                    miniFabColor.startAnimation(fab_open5);
                    miniFabBrightness.startAnimation(fab_open4);
                    miniFabPower.startAnimation(fab_open3);
                    miniFabSetting.startAnimation(fab_open2);
                    miniFabRefresh.setClickable(true);
                    miniFabColor.setClickable(true);
                    miniFabBrightness.setClickable(true);
                    miniFabPower.setClickable(true);
                    miniFabSetting.setClickable(true);
                    if (ArduinoDataModel.getInstance(activity).whichAreChecked().size() == ArduinoDataModel.getInstance(activity).getArduinos().size()) {
                        miniFabDeselect.startAnimation(fab_open);
                        miniFabDeselect.setClickable(true);
                    } else {
                        miniFabSelectAll.startAnimation(fab_open);
                        miniFabSelectAll.setClickable(true);
                    }
                } else {
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        refresh.setMarginEnd(getPixelFromDP(16));
                    else
                        refresh.setMargins(0, 0, getPixelFromDP(16), getPixelFromDP(230));
                    miniFabRefresh.setLayoutParams(refresh);
                    miniFabRefresh.startAnimation(fab_open3);
                    miniFabAdd.startAnimation(fab_open2);
                    miniFabSelectAll.startAnimation(fab_open);
                    miniFabRefresh.setClickable(true);
                    miniFabAdd.setClickable(true);
                    miniFabSelectAll.setClickable(true);
                }
                isFabOpen = true;
            }
        } else if (bottomNav.getSelectedItemId() == R.id.action_mhz) {
            if (isFabOpen) {
                miniFabPower.clearAnimation();
                miniFabBrightness.clearAnimation();
                miniFabColor.clearAnimation();
                fab.startAnimation(rotate_backward);
                if (MhzDataModel.getInstance(activity).whichAreChecked().size() > 0) {
                    miniFabRefresh.startAnimation(fab_close6);
                    miniFabPower.startAnimation(fab_close5);
                    miniFabSetting.startAnimation(fab_close4);
                    miniFabRefresh.setClickable(false);
                    miniFabPower.setClickable(false);
                    miniFabSetting.setClickable(false);
                    if (MhzDataModel.getInstance(activity).whichAreChecked().size() == MhzDataModel.getInstance(activity).getMhzs().size()) {
                        miniFabDeselect.startAnimation(fab_close3);
                        miniFabDeselect.setClickable(false);
                    } else {
                        miniFabSelectAll.startAnimation(fab_close3);
                        miniFabSelectAll.setClickable(false);
                    }

                } else {
                    miniFabSetting.clearAnimation();

                    miniFabRefresh.startAnimation(fab_close6);
                    miniFabAdd.startAnimation(fab_close5);
                    miniFabSelectAll.startAnimation(fab_close4);
                    miniFabRefresh.setClickable(false);
                    miniFabAdd.setClickable(false);
                    miniFabSelectAll.setClickable(false);
                }
                isFabOpen = false;

            } else {
                fab.startAnimation(rotate_forward);
                int orientation = activity.getResources().getConfiguration().orientation;
                if (MhzDataModel.getInstance(activity).whichAreChecked().size() > 0) {
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        refresh.setMargins(0, 0, getPixelFromDP(86), getPixelFromDP(90));
                    else
                        refresh.setMargins(0, 0, getPixelFromDP(16), getPixelFromDP(300));
                    miniFabRefresh.setLayoutParams(refresh);
                    miniFabRefresh.startAnimation(fab_open4);
                    miniFabPower.startAnimation(fab_open3);
                    miniFabSetting.startAnimation(fab_open2);
                    miniFabRefresh.setClickable(true);
                    miniFabPower.setClickable(true);
                    miniFabSetting.setClickable(true);
                    if (MhzDataModel.getInstance(activity).whichAreChecked().size() == MhzDataModel.getInstance(activity).getMhzs().size()) {
                        miniFabDeselect.startAnimation(fab_open);
                        miniFabDeselect.setClickable(true);
                    } else {
                        miniFabSelectAll.startAnimation(fab_open);
                        miniFabSelectAll.setClickable(true);
                    }
                } else {
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE)
                        refresh.setMarginEnd(getPixelFromDP(16));
                    else
                        refresh.setMargins(0, 0, getPixelFromDP(16), getPixelFromDP(230));
                    miniFabRefresh.setLayoutParams(refresh);
                    miniFabRefresh.startAnimation(fab_open3);
                    miniFabAdd.startAnimation(fab_open2);
                    miniFabSelectAll.startAnimation(fab_open);
                    miniFabRefresh.setClickable(true);
                    miniFabAdd.setClickable(true);
                    miniFabSelectAll.setClickable(true);

                }
                isFabOpen = true;
            }
        }
    }

    protected void showFab() {
        if (bottomNav.getSelectedItemId() == R.id.action_settings) {
            fab.startAnimation(fab_open_main);
            fab.setClickable(true);
        }
    }

    protected void hideFab() {
        fab.startAnimation(fab_close_main);
        fab.setClickable(false);
    }

    private int getPixelFromDP(int dp) {
        Resources r = activity.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
    }
}
