/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package michael.ledcontrol.ui.settings;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ColorPresets;
import michael.ledcontrol.backend.NetworkStateTask;
import michael.ledcontrol.backend.SettingsDataModel;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * SettingsFragment is used to manage all application settings via UI
 *
 * @author Michael Schmitz
 */
public class SettingsFragment extends Fragment {

    /**
     * Default implementation, inflates the view
     * @param inflater inflater
     * @param container container
     * @param savedInstanceState savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_fragment, container, false);
    }

    /**
     * loads the content of the fragment (preferences and layout for these)
     * @param view current view
     * @param savedInstanceState savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        MainPreferenceFragment prefFragment = (MainPreferenceFragment) fm.findFragmentByTag("prefFrag");
        if (prefFragment == null) {
            prefFragment = new MainPreferenceFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.pref_content, prefFragment, "prefFrag");
            ft.commit();
            fm.executePendingTransactions();
        }
    }

    /**
     * Class to manage the loaded preferences
     */
    public static class MainPreferenceFragment extends android.preference.PreferenceFragment {

        /**
         * loads all preferences, creates listeners and sets summaries
         * @param savedInstanceState savedInstanceState
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);
            findPreference("primaryColour").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int color = (int) newValue;
                    ((MainActivity) getActivity()).setColor(color);
                    SettingsDataModel.getInstance((MainActivity) getActivity()).setColour(color);
                    return true;
                }
            });

            Preference refreshPref = findPreference("refreshrate");
            refreshPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SettingsDataModel settingsDataModel = SettingsDataModel.getInstance((MainActivity) getActivity());
                    int refreshrate = Integer.parseInt((String) newValue);
                    int refreshrateOld = settingsDataModel.getRefreshrate();
                    settingsDataModel.setRefreshrate(refreshrate);
                    NetworkStateTask.getInstance((MainActivity) getActivity(), refreshrateOld).restartTask(settingsDataModel.getRefreshrate());
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue((String) newValue);
                    preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
                    return true;
                }
            });
            ListPreference listPreference = (ListPreference) refreshPref;
            int index = listPreference.findIndexOfValue(  PreferenceManager
                    .getDefaultSharedPreferences(refreshPref.getContext())
                    .getString(refreshPref.getKey(), ""));
            refreshPref.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);


            prefChangeUpdateColorPref("color01");
            prefChangeUpdateColorPref("color02");
            prefChangeUpdateColorPref("color03");
            prefChangeUpdateColorPref("color04");
            prefChangeUpdateColorPref("color05");
            prefChangeUpdateColorPref("color06");
            prefChangeUpdateColorPref("color07");
            prefChangeUpdateColorPref("color08");
            prefChangeUpdateColorPref("color09");
            prefChangeUpdateColorPref("color10");
            prefChangeUpdateColorPref("color11");
            prefChangeUpdateColorPref("color12");
            prefChangeUpdateColorPref("color13");
            prefChangeUpdateColorPref("color14");
            prefChangeUpdateColorPref("color15");
            prefChangeUpdateColorPref("color16");
            prefChangeUpdateColorPref("color17");
            prefChangeUpdateColorPref("color18");
            prefChangeUpdateColorPref("color19");

            bindSummaryToValue(findPreference("mhz_name"));
            bindSummaryToValue(findPreference("mhz_ip"));
            bindSummaryToValue(findPreference("mhz_net"));
            bindSummaryToValue(findPreference("mhz_device"));
            bindSummaryToValue(findPreference("arduino_name"));
            bindSummaryToValue(findPreference("arduino_ip"));

            findPreference("arduino_color").setOnPreferenceChangeListener(preferenceChangeListener);
            findPreference("arduino_power").setOnPreferenceChangeListener(preferenceChangeListener);
            findPreference("arduino_brightness").setOnPreferenceChangeListener(preferenceChangeListener);
        }

        /**
         * Sets the current value of the preference as summary
         * @param preference preference to bind the summary to
         */
        private void bindSummaryToValue(Preference preference) {
            preference.setOnPreferenceChangeListener(bindPreferenceSummaryToValueListener);
            preference.setSummary(PreferenceManager
                    .getDefaultSharedPreferences(preference.getContext())
                    .getString(preference.getKey(), ""));
        }

        /**
         * Creates a simple listener, that updates the SettingsDataModel, when a preference is changed
         */
        private Preference.OnPreferenceChangeListener preferenceChangeListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SettingsDataModel.getInstance((MainActivity) getActivity()).updatePreference(preference.getKey(), newValue);
                return true;
            }
        };

        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private Preference.OnPreferenceChangeListener bindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                SettingsDataModel.getInstance((MainActivity) getActivity()).updatePreference(preference.getKey(), newValue);
                return true;
            }
        };

        /**
         * Creates a listener on the preference key, updates the color in the DataModel, if the preset preference is changed
         * @param key name of Preference
         */
        private void prefChangeUpdateColorPref(String key) {
            findPreference(key).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ColorPresets.getInstance((MainActivity) getActivity()).updatePresets((MainActivity) getActivity());
                    SettingsDataModel.getInstance((MainActivity) getActivity()).updatePreferenceColor(preference.getKey(), newValue);
                    return true;
                }
            });
        }
    }
}
