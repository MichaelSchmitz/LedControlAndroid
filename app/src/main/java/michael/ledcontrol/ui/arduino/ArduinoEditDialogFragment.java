/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.arduino;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorShape;

import java.util.ArrayList;
import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.ColorConverter;
import michael.ledcontrol.backend.ColorPresets;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.Network;
import michael.ledcontrol.backend.SettingsDataModel;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.main.Snackbar;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * DialogFragment which inflates the arduino.xml layout.
 * Use this to modify selected Arduinos or add a new one to your list
 * Call only if at least one Arduino is selected, otherwise the default is modified
 * IMPORTANT: use setArgument(boolean add) before you show the Fragment, otherwise its always false
 *
 * @author Michael Schmitz
 */

public class ArduinoEditDialogFragment extends DialogFragment {
    private TextView colourEdit;
    private int color = Color.BLACK;
    private boolean add = false;
    private List<Integer> selected;
    private MainActivity activity;

    /**
     * Setter for the private property
     *
     * @param add true, if you want to add a new Arduino, else to modify existing ones
     */
    public void setArgument(boolean add) {
        this.add = add;
    }

    /**
     * Opens a new ArduinoEditDialogFragment
     *
     * @param add specifies wether the Fragment should add or edit Arduinos
     */
     public static void arduinoEdit(boolean add, FragmentManager fragmentManager) {
        ArduinoEditDialogFragment arduinoEditDialogFragment = new ArduinoEditDialogFragment();
        arduinoEditDialogFragment.setArgument(add);
        arduinoEditDialogFragment.show(fragmentManager, "arduinoEdit");
    }


    /**
     * Sets the text and hint, applies the colour to the text at the TextView in the AlertDialog
     *
     * @param id    ResourceId, has to be 0, if text should be used
     * @param text  Custom text, used when id == 0, text shown in the TextView
     * @param hint  hint text, used for storing in database
     * @param color Color-Int that should be used for the text
     */
    public void updatePopupColour(int id, String text, String hint, int color) {
        if (colourEdit != null) {
            if (id == 0)
                colourEdit.setText(text);
            else
                colourEdit.setText(id);
            colourEdit.setHint(hint);
            colourEdit.setTextColor(color);
            this.color = color;
        }
    }

    /**
     * Saves the value of add and the List selected
     *
     * @param outstate used for storing values over view states
     */
    @Override
    public void onSaveInstanceState(Bundle outstate) {
        outstate.putBoolean("add", add);
        outstate.putIntegerArrayList("selected", (ArrayList<Integer>) selected);
    }

    /**
     * TODO: modularize
     * Creates the AlertDialog with all its listeners
     * Reads the stored values, if there are any, and sets them again
     * uses the {@link android.text.Layout arduino.xml} for the Dialog.
     * Sets hints according to the default stored in the Database on adding
     * Sets the texts according to the selected items, on multi selection the first values get set
     * and name and ip get disabled, on no selection the default is selected and can be changed
     *
     * @param savedInstanceState stored values, null if none are stored
     * @return created Dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //load instance values or default
        activity = (MainActivity) getActivity();
        if (savedInstanceState != null) {
            add = savedInstanceState.getBoolean("add");
            selected = savedInstanceState.getIntegerArrayList("selected");
        } else
            selected = ArduinoDataModel.getInstance(activity).whichAreChecked();
        //set all Views of AlertView for further access
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popup = inflater.inflate(R.layout.arduino, null);
        colourEdit = popup.findViewById(R.id.arduino_colour);
        final TextView brightness = popup.findViewById(R.id.arduino_brightness);
        final TextView name = popup.findViewById(R.id.arduino_name);
        final TextView ip = popup.findViewById(R.id.arduino_ip);
        final SeekBar seekBar = popup.findViewById(R.id.arduino_brightnessbar);
        final RadioButton button0 = popup.findViewById(R.id.nPO);
        final RadioButton button1 = popup.findViewById(R.id.PO);
        final RadioButton button2 = popup.findViewById(R.id.PS);
        final Arduino defaultArduino = SettingsDataModel.getInstance(activity).getArduino();
        ImageButton imageButton = popup.findViewById(R.id.arduino_colourpicker);

        // The replace approach is used to support 3rd party keyboards, etc. While still using the easier to input Numbers Keyboard
        ip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(getResources().getString(R.string.multiple_choices))) {
                    if (s.toString().contains(","))
                        s.replace(0, s.length(), new SpannableStringBuilder(s.toString().replace(',', ':')));

                    if (!Network.validateIP(s.toString()))
                        ip.setTextColor(Color.RED);
                    else
                        ip.setTextColor(Color.BLACK);
                }

            }
        });
        color = Color.BLACK;
        if (selected.size() > 0) {
            String col = ArduinoDataModel.getInstance(activity).getArduinos().get(selected.get(0)).colour;
            if (col.matches("#[0-9a-fA-F]{6}"))
                color = Color.parseColor(col);
        }
        //opens ColorPickerDialog for TextView and ImageButton, hides software keyboard if enabled
        colourEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getActivity().getApplication().getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == v) ? null : v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                ColorPickerDialog.newBuilder().setDialogType(ColorPickerDialog.TYPE_PRESETS)
                        .setColorShape(ColorShape.CIRCLE)
                        .setAllowPresets(true)
                        .setAllowCustom(true)
                        .setShowModes(true)
                        .setColor(color)
                        .setPresets(ColorPresets.getInstance(activity).getPresets())
                        .setDialogId(1).show(getActivity());
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getActivity().getApplication().getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == v) ? null : v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                ColorPickerDialog.newBuilder().setDialogType(ColorPickerDialog.TYPE_PRESETS)
                        .setColorShape(ColorShape.CIRCLE)
                        .setAllowPresets(true)
                        .setAllowCustom(true)
                        .setShowModes(true)
                        .setColor(color)
                        .setPresets(ColorPresets.getInstance(activity).getPresets())
                        .setDialogId(1).show(getActivity());
            }
        });
        //sets the values of the various views to the default saved in the Database, if the edit for the default is chosen,
        // not only the hints but also the text (name, ip) is set
        // or if an Arduino was selected to the values of this arduinos (multiple Arduinos -> first)
        if (add) {
            seekBar.setProgress(defaultArduino.brightness);
            brightness.setText(String.valueOf(defaultArduino.brightness));
            setColour(defaultArduino.colour);
            name.setHint(defaultArduino.name);
            ip.setHint(defaultArduino.ip);
            setPower(defaultArduino.power, button0, button1, button2);
        } else {
            Arduino arduino = ArduinoDataModel.getInstance(activity).getArduinos().get(selected.get(0));
            if (selected.size() > 1) {
                name.setText(R.string.multiple_choices);
                ip.setText(R.string.multiple_choices);
                name.setEnabled(false);
                ip.setEnabled(false);
            } else {
                name.setText(arduino.name);
                ip.setText(arduino.ip);
            }
            seekBar.setProgress(arduino.brightness);
            brightness.setText(String.valueOf(arduino.brightness));
            setColour(arduino.colour);
            setPower(arduino.power, button0, button1, button2);
        }
        builder.setView(popup);
        if (add)
            builder.setPositiveButton(R.string.add, null);
        else
            builder.setPositiveButton(R.string.update, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // tracks movement on the brightness slider and adjusts the TextView accordingly
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()

        {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                brightness.setText(String.valueOf(progress));

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
        //not via PositiveButton Listener because some processing is to be done and
        // if an error occurs like user did not input sth it shows an error and returns to the AlertDialog,
        // normal behavior would be to close the dialog
        // Writes all the changes the user made to ArduinoDataModel.getInstance(activity) (either add or update), shows errors if sth went wrong in the Database-Operation
        Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String arduinoName = String.valueOf(name.getText());
                if (arduinoName.equals("")) {
                    Toast.makeText(getContext(), R.string.no_name, Toast.LENGTH_SHORT).show();
                    return;
                }
                String arduinoIp = String.valueOf(ip.getText());
                if (arduinoIp.equals("")) {
                    Toast.makeText(getContext(), R.string.no_ip, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!arduinoIp.equals(getContext().getString(R.string.multiple_choices)) && !Network.validateIP(arduinoIp)) {
                    Toast.makeText(getContext(), R.string.no_ip, Toast.LENGTH_SHORT).show();
                    ip.setTextColor(Color.RED);
                    return;
                }
                Arduino.Power arduinoPower;
                if (button1.isChecked())
                    arduinoPower = Arduino.Power.On;
                else if (button2.isChecked())
                    arduinoPower = Arduino.Power.Standby;
                else if (button0.getTag().equals("S"))
                    arduinoPower = Arduino.Power.noPowerStandby;
                else
                    arduinoPower = Arduino.Power.noPowerOn;
                int arduinoBrightness = Integer.parseInt(String.valueOf(brightness.getText()));
                String arduinoColour = String.valueOf(colourEdit.getHint());

                if (add) {
                    if (!ArduinoDataModel.getInstance(activity).add(new Arduino(arduinoName, arduinoIp, arduinoPower, arduinoBrightness, arduinoColour)))
                        Snackbar.makeSnackbar(R.string.add_error, activity);
                } else {
                    if (selected.size() > 1) {
                        for (int i : selected) {
                            ArduinoDataModel.getInstance(activity).setBrightness(arduinoBrightness, i);
                            ArduinoDataModel.getInstance(activity).setPower(arduinoPower, i);
                            ArduinoDataModel.getInstance(activity).setColour(arduinoColour, i);
                            ArduinoDataModel.getInstance(activity).setChecked(false, i);
                        }
                    } else if (selected.size() == 1) {
                        if (!ArduinoDataModel.getInstance(activity).update(new Arduino(ArduinoDataModel.getInstance(activity).getArduinos().get(selected.get(0)).id, arduinoName, arduinoIp, arduinoPower, arduinoBrightness, arduinoColour), selected.get(0)))
                            Snackbar.makeSnackbar(R.string.update_error, activity);
                        ArduinoDataModel.getInstance(activity).setChecked(false, selected.get(0));
                    } else {
                        if (!ArduinoDataModel.getInstance(activity).update(new Arduino(defaultArduino.id, arduinoName, arduinoIp, arduinoPower, arduinoBrightness, arduinoColour), -1))
                            Snackbar.makeSnackbar(R.string.update_error, activity);
                    }
                }
                dialog.dismiss();
            }
        });
        return dialog;
    }

    /**
     * Activates the corresponding button for all power states
     * Sets the Tag for noPowerState
     *
     * @param power Power after which the buttons are to be set
     * @param nPO   noPowerOn Button
     * @param O     Power Off Button
     * @param S     Power On Button
     */
    public void setPower(Arduino.Power power, RadioButton nPO, RadioButton O, RadioButton S) {
        switch (power) {
            case noPowerStandby: {
                nPO.setTag("S");
                nPO.toggle();
                break;
            }
            case noPowerOn: {
                nPO.setTag("O");
                nPO.toggle();
                break;
            }
            case On: {
                O.toggle();
                break;
            }
            case Standby: {
                S.toggle();
                break;
            }
            default: {
                O.toggle();
            }
        }
    }

    /**
     * sets the colourEdit TextView according to colour, differences between modes
     *
     * @param colour ColorConverter to set
     */
    public void setColour(String colour) {
        switch (colour) {
            case "rainbow": {
                colourEdit.setText(R.string.rainbow);
                colourEdit.setHint("rainbow");
                colourEdit.setTextColor(Color.BLACK);
                break;
            }
            case "rainbow_circle": {
                colourEdit.setText(R.string.rainbow_circle);
                colourEdit.setHint("rainbow_circle");
                colourEdit.setTextColor(Color.BLACK);
                break;
            }
            case "rainbow_sinus": {
                colourEdit.setText(R.string.rainbow_sinus);
                colourEdit.setHint("rainbow_sinus");
                colourEdit.setTextColor(Color.BLACK);
                break;
            }
            case "spectrum": {
                colourEdit.setText(R.string.spectrum);
                colourEdit.setHint("spectrum");
                colourEdit.setTextColor(Color.BLACK);
                break;
            }
            case "spectrum_sinus": {
                colourEdit.setText(R.string.spectrum_sinus);
                colourEdit.setHint("spectrum_sinus");
                colourEdit.setTextColor(Color.BLACK);
                break;
            }
            default: {
                colourEdit.setText(ColorConverter.getNameByHex(colour));
                colourEdit.setHint(colour);
                colourEdit.setTextColor(Color.parseColor(colour));
            }
        }
    }
}
