/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.arduino;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.ui.main.MainActivity;

/**
 * DialogFragment which inflates the application_slider.xml layout
 * Use this to open a new AlertView with a BrightnessSlider
 *
 * Call only if at least one Arduino is selected
 *
 * @author Michael Schmitz
 */

public class ArduinoBrightnessDialogFragment extends DialogFragment {
    private int rowID;

    /**
     * Used to set id (int) of the arduino to modify
     * @param args Bundle with one int
     */
    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        rowID = args.getInt("id");
    }

    /**
     * Creates the AlertDialog with the slider and the positive and negative Buttons
     * Loads initial values from the selected object
     * @param savedInstanceState stored values, null if none are stored
     * @return created dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alert = inflater.inflate(R.layout.application_slider, null);
        final TextView textView = alert.findViewById(R.id.seekBarPercentage);
        SeekBar seekBar = alert.findViewById(R.id.seekBar);
        //set the startValues to current Values (of first or only selected element)
        seekBar.setProgress(ArduinoDataModel.getInstance((MainActivity) getActivity()).getArduinos().get(rowID).brightness);
        textView.setText(String.valueOf(ArduinoDataModel.getInstance((MainActivity) getActivity()).getArduinos().get(rowID).brightness));
        builder.setView(alert);
        //tracks the userMovement on the slider and sets the TextView accordingly
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                textView.setText(String.valueOf(progress));
            }
        });
        builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    ArduinoDataModel.getInstance((MainActivity) getActivity()).setChecked(false, rowID);
                    ArduinoDataModel.getInstance((MainActivity) getActivity()).setBrightness(Integer.parseInt(String.valueOf(textView.getText())), rowID);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

}
