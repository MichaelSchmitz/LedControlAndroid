/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.arduino;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.backend.NetworkStateTask;
import michael.ledcontrol.backend.SettingsDataModel;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.main.SimpleTouchCallbackHandler;
import michael.ledcontrol.ui.main.Snackbar;

/**
 * Fragment which contains the RecyclerView for displaying all the Arduinos,
 * implements a Swipe Left Handler for Item deletion
 *
 * @author Michael Schmitz
 */
public class ArduinoFragment extends Fragment implements SimpleTouchCallbackHandler.RecyclerItemTouchHelperListener {
    private ArduinoRecyclerView mAdapter;
    private MainActivity activity;

    /**
     * Creates an instance of ArduinoRecylcerView
     * @param savedInstanceState savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        mAdapter = new ArduinoRecyclerView(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.arduino_fragment, container, false);

        final SwipeRefreshLayout refreshLayout = v.findViewById(R.id.swiperefresh);
        final RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        ArduinoDataModel.getInstance(activity).setRecyclerView(mAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        ItemTouchHelper.SimpleCallback swipeToDeleteCallback = new SimpleTouchCallbackHandler(
                ItemTouchHelper.UP + ItemTouchHelper.DOWN, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(swipeToDeleteCallback).attachToRecyclerView(recyclerView);
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        NetworkStateTask.getInstance(activity, SettingsDataModel.getInstance(activity).getRefreshrate())
                                .pingOnce(activity, refreshLayout, true);
                    }
                }
        );
        return v;
    }

    /**
     * callback when element in RecyclerView is swiped
     * item will be removed on swiped
     *
     * @param viewHolder current ViewHolder
     * @param direction  direction of swipe
     * @param position   position of element in RecyclerView
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ArduinoRecyclerView.ViewHolder) {
            if (activity.fabs.getFABState())
                activity.fabs.animateFAB();
            Arduino arduino = ArduinoDataModel.getInstance(activity).getArduinos().get(position);
            ArduinoDataModel.getInstance(activity).delete(position);
            Snackbar.makeSnackbar(R.string.deleted, activity, arduino, position);
        }
    }

    /**
     * callback when element in RecyclerView is dragged
     * Arduinos and ArduinoNetworks are swapped
     *
     * @param fromPos old position of element
     * @param toPos   new position of element
     */
    @Override
    public void onMove(int fromPos, int toPos) {
        if (fromPos < toPos) {
            for (int i = fromPos; i < toPos; i++) {
                Collections.swap(ArduinoDataModel.getInstance(activity).getArduinos(), i, i + 1);
                Collections.swap(ArduinoDataModel.getInstance(activity).getNetworks(), i, i + 1);
            }
        } else {
            for (int i = fromPos; i > toPos; i--) {
                Collections.swap(ArduinoDataModel.getInstance(activity).getArduinos(), i, i - 1);
                Collections.swap(ArduinoDataModel.getInstance(activity).getNetworks(), i, i - 1);
            }
        }
        mAdapter.notifyItemMoved(fromPos, toPos);
    }
}