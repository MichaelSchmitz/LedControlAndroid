/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.arduino;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorShape;

import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.ArduinoDataModel;
import michael.ledcontrol.backend.ColorConverter;
import michael.ledcontrol.backend.ColorPresets;
import michael.ledcontrol.backend.Database.Arduino;
import michael.ledcontrol.ui.main.FAB;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.main.ViewHolderInterface;

/**
 * ArduinoRecyclerView (RecyclerView.Adapter) to show the data from ArduinoDataModel
 *
 * @author Michael Schmitz
 */
public class ArduinoRecyclerView extends RecyclerView.Adapter<ArduinoRecyclerView.ViewHolder> {
    private final List<Arduino> values;
    private final MainActivity activity;
    private FAB fabs;

    /**
     * Returns the activity (used only for SimpleTouchCallbackHandler) else please use DI
     * @return activity
     */
    public MainActivity getActivity() {
        return activity;
    }

    /**
     * creates the recyclerView, loads all values, the fab
     * @param activity needed to load the values
     */
    public ArduinoRecyclerView(MainActivity activity) {
        this.values = ArduinoDataModel.getInstance(activity).getArduinos();
        this.activity = activity;
        this.fabs = activity.fabs;
    }

    /**
     * Creates a viewHolder with the rowLayout for arduinos
     * @param parent parent ViewGroup
     * @param viewType viewType
     * @return inflated viewHolder
     */
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.arduino_recyclerview_rowlayout, parent, false);

        return new ViewHolder(itemView);
    }

    /**
     * sets all Views corresponding to the values obtained from ArduinoDataModel for the
     * current position, also sets a Listener on the rowView to check the element if clicked upon
     * (also a check if all are clicked to update the selectAll/deselect MenuItems)
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Arduino arduino = values.get(position);

        holder.name.setText(arduino.name);
        if (arduino.power == Arduino.Power.On) {
            holder.aSwitch.setChecked(true);
            holder.aSwitch.setEnabled(true);
            holder.power.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_green_dark)));
        }
        else if (arduino.power == Arduino.Power.Standby) {
            holder.aSwitch.setChecked(false);
            holder.aSwitch.setEnabled(true);
            holder.power.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_orange_dark)));
        }
        else {
            holder.aSwitch.setEnabled(false);
            holder.power.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_red_dark)));
        }
        holder.brightness.setText(String.valueOf(arduino.brightness));
        holder.checkBox.setChecked(arduino.checked);
        switch (arduino.colour) {
            case "rainbow": {
                holder.colour.setText(R.string.rainbow);
                holder.colour.setTextColor(Color.BLACK);
                break;
            }
            case "rainbow_circle": {
                holder.colour.setText(R.string.rainbow_circle);
                holder.colour.setTextColor(Color.BLACK);
                break;
            }
            case "rainbow_sinus": {
                holder.colour.setText(R.string.rainbow_sinus);
                holder.colour.setTextColor(Color.BLACK);
                break;
            }
            case "spectrum": {
                holder.colour.setText(R.string.spectrum);
                holder.colour.setTextColor(Color.BLACK);
                break;
            }
            case "spectrum_sinus": {
                holder.colour.setText(R.string.spectrum_sinus);
                holder.colour.setTextColor(Color.BLACK);
                break;
            }
            default: {
                holder.colour.setText(ColorConverter.getNameByHex(values.get(position).colour));
                holder.colour.setTextColor(Color.parseColor(values.get(position).colour));
            }
        }
        holder.foreground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state = fabs.getFABState();
                if (holder.checkBox.isChecked()) {
                    if (state) //TODO: If already one other selected  do  not reopen because anim should not be displayed
                        fabs.animateFAB();
                    ArduinoDataModel.getInstance(activity).setChecked(false, holder.getAdapterPosition());
                    if (state)
                        fabs.animateFAB();
                }
                else {
                    if (state)
                        fabs.animateFAB();
                    ArduinoDataModel.getInstance(activity).setChecked(true, holder.getAdapterPosition());
                    if (state)
                        fabs.animateFAB();
                }
            }
        });
        holder.aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.aSwitch.isChecked())
                    ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.On, holder.getAdapterPosition());
                else
                    ArduinoDataModel.getInstance(activity).setPower(Arduino.Power.Standby, holder.getAdapterPosition());
            }
        });
        holder.brightnessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("id", holder.getAdapterPosition());
                ArduinoBrightnessDialogFragment arduinoBrightnessDialogFragment = new ArduinoBrightnessDialogFragment();
                arduinoBrightnessDialogFragment.setArguments(args);
                arduinoBrightnessDialogFragment.show(activity.getFragmentManager(), "brightness");
            }
        });
        holder.colourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialog.newBuilder().setDialogType(ColorPickerDialog.TYPE_PRESETS)
                        .setColorShape(ColorShape.CIRCLE)
                        .setAllowCustom(true)
                        .setAllowPresets(true)
                        .setShowModes(true)
                        .setPresets(ColorPresets.getInstance(activity).getPresets())
                        .setDialogId(holder.getAdapterPosition() + 3).show(activity);
            }
        });
    }

    /**
     * returns the total number of rows
     *
     * @return size of displayed list
     */
    @Override
    public int getItemCount() {
        return values == null ? 0 : values.size() ;
    }


    /**
     * ViewHolder implementation for easier and less resource demanding management of the RecyclerView
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements ViewHolderInterface {
        private RelativeLayout foreground, background;
        private CheckBox checkBox;
        private Switch aSwitch;
        private TextView name, brightness, colour;
        private ImageButton brightnessBtn, colourBtn;
        private ImageView power;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            brightness = view.findViewById(R.id.textBrightness);
            brightnessBtn = view.findViewById(R.id.arduino_brightness_button);
            colour = view.findViewById(R.id.textColour);
            colourBtn = view.findViewById(R.id.arduino_colour_button);
            power = view.findViewById(R.id.statusPower);
            aSwitch = view.findViewById(R.id.arduino_power_switch);
            checkBox = view.findViewById(R.id.checkBox);
            foreground = view.findViewById(R.id.row_foreground);
            background = view.findViewById(R.id.row_background);
        }

        public RelativeLayout getForeground() {
            return foreground;
        }

        public RelativeLayout getBackground() {
            return background;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

    }
}

