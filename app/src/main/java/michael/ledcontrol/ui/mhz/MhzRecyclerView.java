/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.mhz;

import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.backend.MhzDataModel;
import michael.ledcontrol.ui.main.FAB;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.main.ViewHolderInterface;

/**
 * ArduinoRecyclerView (RecyclerView.Adapter) to show the data from ArduinoDataModel
 *
 * @author Michael Schmitz
 */
public class MhzRecyclerView extends RecyclerView.Adapter<MhzRecyclerView.ViewHolder> {
    private final List<Mhz> values;
    private final MainActivity activity;
    private FAB fabs;

    /**
     * Returns the activity (used only for SimpleTouchCallbackHandler) else please use DI
     * @return activity
     */
    public MainActivity getActivity() {
        return activity;
    }

    /**
     * creates the recyclerView, loads all values, the fab
     * @param activity needed to load the values
     */
    public MhzRecyclerView(MainActivity activity) {
        this.values = MhzDataModel.getInstance(activity).getMhzs();
        this.activity = activity;
        this.fabs = activity.fabs;
    }

    /**
     * Creates a viewHolder with the rowLayout for mhzDevices
     * @param parent parent ViewGroup
     * @param viewType viewType
     * @return inflated viewHolder
     */
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mhz_recyclerview_rowlayout, parent, false);

        return new ViewHolder(itemView);
    }

    /**
     * sets all Views corresponding to the values obtained from ArduinoDataModel for the
     * current position, also sets a Listener on the rowView to check the element if clicked upon
     * (also a check if all are clicked to update the selectAll/deselect MenuItems)
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Mhz mhz = values.get(position);

        holder.name.setText(mhz.name);
        holder.checkBox.setChecked(mhz.checked);
        if (mhz.state) {
            holder.aSwitch.setChecked(true);
            holder.power.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_green_dark)));
        } else {
            holder.aSwitch.setChecked(false);
            holder.power.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_red_dark)));
        }
        if (mhz.serverUp) {
            holder.aSwitch.setEnabled(true);
            holder.server.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_green_dark)));
        } else {
            holder.aSwitch.setEnabled(false);
            holder.server.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(activity, android.R.color.holo_red_dark)));
        }
        holder.foreground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state = fabs.getFABState();
                if (holder.checkBox.isChecked()) {
                    if (state)
                        fabs.animateFAB();
                    MhzDataModel.getInstance(activity).setChecked(false, holder.getAdapterPosition());
                    if (state)
                        fabs.animateFAB();
                }

                else {
                    if (state)
                        fabs.animateFAB();
                    MhzDataModel.getInstance(activity).setChecked(true, holder.getAdapterPosition());
                    if (state)
                        fabs.animateFAB();
                }
            }
        });
        holder.aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    MhzDataModel.getInstance(activity).setPower(holder.aSwitch.isChecked(), holder.getAdapterPosition());
            }
        });
    }

    /**
     * returns the total number of rows
     *
     * @return size of displayed list
     */
    @Override
    public int getItemCount() {
        return values == null ? 0: values.size();
    }


    /**
     * ViewHolder implementation for easier and less resource demanding management of the RecyclerView
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements ViewHolderInterface {
        private RelativeLayout foreground, background;
        private TextView name;
        private ImageView power, server;
        private CheckBox checkBox;
        private Switch aSwitch;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.mhzName);
            checkBox = view.findViewById(R.id.mhzCheckbox);
            power = view.findViewById(R.id.mhzPower);
            server = view.findViewById(R.id.serverPower);
            aSwitch = view.findViewById(R.id.mhz_power_switch);
            foreground = view.findViewById(R.id.mhz_row_foreground);
            background = view.findViewById(R.id.mhz_row_background);
        }

        public RelativeLayout getForeground() {
            return foreground;
        }

        public RelativeLayout getBackground() {
            return background;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

    }
}

