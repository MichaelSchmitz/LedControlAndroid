/*
 * Copyright (C) 2018 Michael Schmitz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package michael.ledcontrol.ui.mhz;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import michael.ledcontrol.R;
import michael.ledcontrol.backend.Database.Mhz;
import michael.ledcontrol.backend.MhzDataModel;
import michael.ledcontrol.backend.Network;
import michael.ledcontrol.backend.SettingsDataModel;
import michael.ledcontrol.ui.main.MainActivity;
import michael.ledcontrol.ui.main.Snackbar;

/**
 * DialogFragment which inflates the mhz.xml layout.
 * Use this to modify selected 433Mhz Devices or add a new one to your list
 *
 * Call only if at least one device is selected, otherwise the default is modified
 *
 * IMPORTANT: use setArgument(boolean add) before you show the Fragment, otherwise its always false
 *
 * @author Michael Schmitz
 */

public class MhzEditDialogFragment extends DialogFragment {
    private boolean add = false;
    private List<Integer> selected;
    private MainActivity activity;

    /**
     * Setter for the private property
     *
     * @param add true, if you want to add a new 433Mhz device, else to modify existing ones
     */
    public void setArgument(boolean add) {
        this.add = add;
    }

    /**
     * Opens a new ArduinoEditDialogFragment
     *
     * @param add specifies wether the Fragment should add or edit Arduinos
     */
    public static void mhzEdit(boolean add, FragmentManager fragmentManager) {
        MhzEditDialogFragment mhzEditDialogFragment = new MhzEditDialogFragment();
        mhzEditDialogFragment.setArgument(add);
        mhzEditDialogFragment.show(fragmentManager, "MhzEdit");
    }

    /**
     * Saves the value of add and the List selected
     *
     * @param outstate used for storing values over view states
     */
    @Override
    public void onSaveInstanceState(Bundle outstate) {
        outstate.putBoolean("add", add);
        outstate.putIntegerArrayList("selected", (ArrayList<Integer>) selected);
    }

    /**
     * TODO: modularize
     * Creates the AlertDialog with all its listeners
     * Reads the stored values, if there are any, and sets them again
     * uses the {@link android.text.Layout arduino.xml} for the Dialog.
     * Sets hints according to the default stored in the Database on adding
     * Sets the texts according to the selected items, on multi selection the first values get set
     * and name and ip get disabled, on no selection the default is selected and can be changed
     *
     * @param savedInstanceState stored values, null if none are stored
     * @return created Dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //load instance values or default
        activity = (MainActivity) getActivity();
        if (savedInstanceState != null) {
            add = savedInstanceState.getBoolean("add");
            selected = savedInstanceState.getIntegerArrayList("selected");
        } else
            selected = MhzDataModel.getInstance(activity).whichAreChecked();
        //set all Views of AlertView for further access
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popup = inflater.inflate(R.layout.mhz, null);
        final EditText name = popup.findViewById(R.id.mhz_name);
        final EditText ip = popup.findViewById(R.id.mhz_ip);
        final EditText net = popup.findViewById(R.id.mhz_net);
        final EditText device = popup.findViewById(R.id.mhz_device);
        final Mhz defaultMhz = SettingsDataModel.getInstance(activity).getMhz();

        // The replace approach is used to support 3rd party keyboards, etc. While still using the easier to input Numbers Keyboard
        ip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(getResources().getString(R.string.multiple_choices))) {
                    if (s.toString().contains(","))
                        s.replace(0, s.length(), new SpannableStringBuilder(s.toString().replace(',', ':')));

                    if (!Network.validateIP(s.toString()))
                        ip.setTextColor(Color.RED);
                    else
                        ip.setTextColor(Color.BLACK);
                }

            }
        });
        device.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(getResources().getString(R.string.multiple_choices))) {
                    if (s.length() != 5)
                        device.setTextColor(Color.RED);
                    else
                        device.setTextColor(Color.BLACK);
                }
            }
        });
        net.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() != 5)
                    net.setTextColor(Color.RED);
                else
                    net.setTextColor(Color.BLACK);
            }
        });
        //sets the values of the various views to the default saved in the Database, if the edit for the default is chosen,
        // not only the hints but also the text (name, ip) is set
        // or if an Arduino was selected to the values of this arduinos (multiple Arduinos -> first)
        if (add) {
            name.setHint(defaultMhz.name);
            ip.setHint(defaultMhz.ip);
            net.setHint(defaultMhz.net);
            device.setHint(defaultMhz.device);
        } else {
            Mhz mhz = MhzDataModel.getInstance(activity).getMhzs().get(selected.get(0));
            if (selected.size() > 1) {
                device.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
                name.setText(R.string.multiple_choices);
                device.setText(R.string.multiple_choices);
                name.setEnabled(false);
                device.setEnabled(false);
            } else {
                name.setText(mhz.name);
                device.setText(mhz.device);
            }
            net.setText(mhz.net);
            ip.setText(mhz.ip);
        }
        builder.setView(popup);
        if (add)
            builder.setPositiveButton(R.string.add, null);
        else
            builder.setPositiveButton(R.string.update, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
        //not via PositiveButton Listener because some processing is to be done and
        // if an error occurs like user did not input sth it shows an error and returns to the AlertDialog,
        // normal behavior would be to close the dialog
        // Writes all the changes the user made to ArduinoDataModel (either add or update), shows errors if sth went wrong in the Database-Operation
        Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String mhzName = String.valueOf(name.getText());
                if (mhzName.equals("")) {
                    Toast.makeText(getContext(), R.string.no_name, Toast.LENGTH_SHORT).show();
                    return;
                }
                String mhzIp = String.valueOf(ip.getText());
                if (mhzIp.equals("")) {
                    Toast.makeText(getContext(), R.string.no_ip, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!mhzIp.equals(getContext().getString(R.string.multiple_choices)) && !Network.validateIP(mhzIp)) {
                    Toast.makeText(getContext(), R.string.no_ip, Toast.LENGTH_SHORT).show();
                    ip.setTextColor(Color.RED);
                    return;
                }
                String mhzNet = String.valueOf(net.getText());
                if (mhzNet.equals("") || mhzNet.length() != 5) {
                    Toast.makeText(getContext(), R.string.no_net, Toast.LENGTH_SHORT).show();
                    return;
                }
                String mhzDevice = String.valueOf(device.getText());
                if (mhzDevice.equals("") || mhzDevice.length() != 5) {
                    Toast.makeText(getContext(), R.string.no_device, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (add) {
                    if (!MhzDataModel.getInstance(activity).add(new Mhz(mhzName, mhzNet, mhzDevice, mhzIp)))
                        Snackbar.makeSnackbar(R.string.add_error, activity);
                } else {
                    if (selected.size() > 1) {
                        for (int i : selected) {
                            MhzDataModel.getInstance(activity).setNet(mhzNet, i);
                            MhzDataModel.getInstance(activity).setIP(mhzIp, i);
                            MhzDataModel.getInstance(activity).setChecked(false, i);
                        }
                    } else if (selected.size() == 1) {
                        if (!MhzDataModel.getInstance(activity).update(new Mhz(MhzDataModel.getInstance(activity).getMhzs().get(selected.get(0)).id, mhzName, mhzNet, mhzDevice, mhzIp), selected.get(0)))
                            Snackbar.makeSnackbar(R.string.update_error, activity);
                        MhzDataModel.getInstance(activity).setChecked(false, selected.get(0));
                    } else {
                        if (!MhzDataModel.getInstance(activity).update(new Mhz(defaultMhz.id, mhzName, mhzNet, mhzDevice, mhzIp), -1))
                            Snackbar.makeSnackbar(R.string.update_error, activity);
                    }
                }
                dialog.dismiss();
            }
        });
        return dialog;
    }
}
