/*
* RGB <--> HSV conversion in integer arithmetics, to be used on Windows.
* Copyright (c) 2013 Martin Mitas
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*/

#include "hsv.h"


#define MIN(a,b)      ((a) < (b) ? (a) : (b))
#define MAX(a,b)      ((a) > (b) ? (a) : (b))

#define MIN3(a,b,c)   MIN((a), MIN((b), (c)))
#define MAX3(a,b,c)   MAX((a), MAX((b), (c)))

void HSV::rgb2hsv(int r, int g, int b, hsv_t* hsv)
{
	int m = MIN3(r, g, b);
	int M = MAX3(r, g, b);
	int delta = M - m;

	if (delta == 0) {
		/* Achromatic case (i.e. grayscale) */
		hsv->hue = -1;          /* undefined */
		hsv->saturation = 0;
	}
	else {
		int h;

		if (r == M)
			h = ((g - b) * 60 * HUE_DEGREE) / delta;
		else if (g == M)
			h = ((b - r) * 60 * HUE_DEGREE) / delta + 120 * HUE_DEGREE;
		else /*if(b == M)*/
			h = ((r - g) * 60 * HUE_DEGREE) / delta + 240 * HUE_DEGREE;

		if (h < 0)
			h += 360 * HUE_DEGREE;

		hsv->hue = h;

		/* The constatnt 8 is tuned to statistically cause as little
		* tolerated mismatches as possible in RGB -> HSV -> RGB conversion.
		*/
		hsv->saturation = (256 * delta - 8) / M;
	}
	hsv->value = M;
}

rgb_t HSV::hsv2rgb(hsv_t* hsv)
{
	byte r, g, b;

	if (hsv->saturation == 0) {
		r = g = b = hsv->value;
	}
	else {
		int h = (hsv->hue)%360;
		int s = hsv->saturation;
		int v = hsv->value;
		int i = h / (60 * HUE_DEGREE);
		int p = (256 * v - s*v) / 256;

		if (i & 1) {
			int q = (256 * 60 * HUE_DEGREE*v - h*s*v + 60 * HUE_DEGREE*s*v*i) / (256 * 60 * HUE_DEGREE);
			switch (i) {
			case 1:   r = q; g = v; b = p; break;
			case 3:   r = p; g = q; b = v; break;
			case 5:   r = v; g = p; b = q; break;
			}
		}
		else {
			int t = (256 * 60 * HUE_DEGREE*v + h*s*v - 60 * HUE_DEGREE*s*v*(i + 1)) / (256 * 60 * HUE_DEGREE);
			switch (i) {
			case 0:   r = v; g = t; b = p; break;
			case 2:   r = p; g = v; b = t; break;
			case 4:   r = t; g = p; b = v; break;
			}
		}
	}
  return rgb_t {r, g, b};
}
