/*
* RGB <--> HSV conversion in integer arithmetics, to be used on Windows.
* Copyright (c) 2013 Martin Mitas
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*/
#include <Arduino.h>
#ifndef HSV_H
#define HSV_H

#define HUE_DEGREE    1

typedef struct hsv_tag hsv_t;
struct hsv_tag {
    int hue;               /* 0 ... (360*HUE_DEGREE - 1) */
    byte saturation;       /* 0 ... 255 */
    byte value;            /* 0 ... 255 */
};
typedef struct rgb_tag rgb_t;
struct rgb_tag {
  int r, g, b;
};
class HSV{

public: void rgb2hsv(int r, int g, int b, hsv_t* hsv);
        rgb_t hsv2rgb(hsv_t* hsv);

};
#endif  /* HSV_H */
