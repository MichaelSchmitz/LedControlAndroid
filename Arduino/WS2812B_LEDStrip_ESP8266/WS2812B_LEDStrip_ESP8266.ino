/*---------------------------------------------------------*\
|                                                           |
|   Modified by Michael Schmitz           28/12/2017        |
|     added Mode and App support                            |
|     functions are taken out of KeyboardVisualizer Project |
|                                                           |
|   WS2812B (NeoPixel) Serial Controlled LED Strip Driver   |
|     for use with Keyboard Visualizer VCUI                 |
|                                                           |
|   Adam Honse (calcprogrammer1) 12/9/2016                  |
|                                                           |
\*---------------------------------------------------------*/

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Adafruit_NeoPixel.h>
#include "hsv.h"

const char* ssid     = "";
const char* password = "";

// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;
HSV colorConverter;

char packet_buf[1024];
unsigned long previousMillis;
float bkgd_step = 0;
bool app = false;
int brightness;
int mode = -1; //-1 not set, 1: rainbow, 2: rainbow sinusdioal, 3: color-Wheel, 4: Spectrum Cycle, 5: Spectrum Cycle Sinusdioal

#define PIN 13
#define LEDS 68
#define PACKET_SZ ( (LEDS * 3) + 3 )
#define PACKET_SZ_APP 8 //0:mode app, 1: lightning mode, 2: brightness, 3: mode or 3-5 color, 6, 7 checksum
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup()
{
  Serial.begin(115200);
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Udp.begin(1234);
  strip.begin();
  previousMillis = millis();  
}

int value = 0;

void loop()
{
  int noBytes = Udp.parsePacket();

  if( noBytes )
  {
    Serial.print("Received ");
    Serial.print(noBytes);
    Serial.print(" bytes\r\n");
    Udp.read(packet_buf, noBytes);
    if(noBytes == PACKET_SZ_APP && packet_buf[0] == 0xBB )
    {
        unsigned short sum = 0;
        int checksum_0 = PACKET_SZ_APP - 2;
        int checksum_1 = PACKET_SZ_APP - 1;

        for( int i = 0; i < checksum_0; i++ )
        {
            sum += packet_buf[i];
        }

        //Test if valid write packet
        if( ( ( (unsigned short)packet_buf[checksum_0] << 8 ) | packet_buf[checksum_1] ) == sum)
              {
                //Color-Mode
                if (packet_buf[1] == 0x11)
                {
                    app = true;
                    mode = 0;
                    for( int i = 0; i < LEDS; i++ )
                    {
                      strip.setPixelColor(i, strip.Color((int)(packet_buf[3] * packet_buf[2] / 100), (int)(packet_buf[4] * packet_buf[2] / 100),(int)(packet_buf[5] * packet_buf[2] / 100)));
                    }
                }
                //Mode-Mode
                else if (packet_buf[1] == 0x22)
                {
                    app = true;
                    brightness = packet_buf[2];
                    mode = packet_buf[3];
                }
                //switch app control off
                else if (packet_buf[1] == 0xAA)
                {
                    app = false;
                    for( int i = 0; i < LEDS; i++ )
                    {
                      strip.setPixelColor(i, strip.Color(0,0,0));
                    }
                }
                strip.show();
              }
    }
    else if(!app && noBytes == PACKET_SZ && packet_buf[0] == 0xAA )
      {
      unsigned short sum = 0;
      int checksum_0 = PACKET_SZ - 2;
      int checksum_1 = PACKET_SZ - 1;

      for( int i = 0; i < checksum_0; i++ )
        {
          sum += packet_buf[i];
        }
        
      //Test if valid write packet
      if( ( ( (unsigned short)packet_buf[checksum_0] << 8 ) | packet_buf[checksum_1] ) == sum )
      {
        for( int i = 0; i < LEDS; i++ )
        {
          int idx = 1 + ( 3 * i );
          
          strip.setPixelColor(i, strip.Color(packet_buf[idx], packet_buf[idx+1], packet_buf[idx+2]));
        }
        previousMillis = millis();
        strip.show();
      }
    }
  }
  else if (app)
  {
    delay(65);
    switch(mode)
    {
        case 1:
        {
          bkgd_step += 15;
          if (bkgd_step >= 360)
            bkgd_step = 0;
  
          DrawRainbow(brightness);
          break;
        }
        case 2:
        {
             bkgd_step += 40;
          if (bkgd_step >= 360)
            bkgd_step = 0;  
          DrawRainbowSinusoidal(brightness);
          break;
        }
        case 3:
        {
          bkgd_step += 10;
          if (bkgd_step >= 360)
            bkgd_step = 0;
          DrawColorWheel(brightness);
          break;
        }
        case 4:
        {
          bkgd_step += 10;
          if (bkgd_step >= 360)
            bkgd_step = 0;
          DrawSpectrumCycle(brightness);
          
          break;
        }
        case 5:
        {
             bkgd_step += 30;
          if (bkgd_step >= 360)
            bkgd_step = 0;  
          DrawSinusoidalCycle(brightness);
          break;
        }
        default: {
            //Do Nothing
        }
    }
  }
  if ((millis() - previousMillis) > 1000 && !app)
  {
    for( int i = 0; i < LEDS; i++ ) 
    {
      strip.setPixelColor(i, strip.Color(0,0,0));
    }
    strip.show(); 
    previousMillis=millis(); 
  }
} 

void DrawRainbow(int bright)
{
    bright = (int)(bright * 2.55f);
    for (int x = 0; x < LEDS; x++)
    {
            int hsv_h = ((int)(bkgd_step + (LEDS + 1 - x)) % 360);
            hsv_t hsv = { hsv_h, 255, bright };
            rgb_t rgb = colorConverter.hsv2rgb(&hsv);            
            strip.setPixelColor(x, strip.Color(rgb.r, rgb.g, rgb.b));       
    }
    strip.show();
}

void DrawRainbowSinusoidal(int bright)
{
    bright = (int)(bright * (255.0f / 100.0f));
    for (int x = 0; x < LEDS; x++)
    {
      int red = (int)((LEDS / 2) * (sin(((((int)((x * (360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f)) + 1));
      int grn = (int)((LEDS / 2) * (sin(((((int)((x * (360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f) - (6.28f / 3)) + 1));
      int blu = (int)((LEDS / 2) * (sin(((((int)((x * (360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f) + (6.28f / 3)) + 1));
      strip.setPixelColor(x, strip.Color((bright * red)/256, (bright * grn)/256, (bright * blu)/256));
    }
    strip.show();
 }

 void DrawColorWheel(int bright)
{
    int center_x = LEDS/2;
    bright = (int)(bright * (255.0f / 100.0f));
    for (int x = 0; x < LEDS; x++)
    {
      float hue = (float)(bkgd_step + ((int)(180 + atan2( - 32, x - center_x) * (180.0 / 3.14159))) % 360);
      hsv_t hsv = { (int)hue, 255, (unsigned char)bright };
      rgb_t rgb = colorConverter.hsv2rgb(&hsv);
      strip.setPixelColor(x, strip.Color(rgb.r, rgb.g, rgb.b));
    }
    strip.show();
}

void DrawSpectrumCycle(int bright)
{
    bright = (int)(bright * (255.0f / 100.0f));
    hsv_t hsv = { (int)bkgd_step, 255, (unsigned char)bright };
    rgb_t rgb = colorConverter.hsv2rgb(&hsv);

    for (int x = 0; x < LEDS; x++)
    {
      strip.setPixelColor(x, strip.Color(rgb.r, rgb.g, rgb.b));
    }
    strip.show();
}

void DrawSinusoidalCycle(int bright)
{
    bright = (int)(bright * (255.0f / 100.0f));
    int red = (int)((LEDS / 2) * (sin(((((int)(((360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f)) + 1));
    int grn = (int)((LEDS / 2) * (sin(((((int)(((360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f) - (6.28f / 3)) + 1));
    int blu = (int)((LEDS / 2) * (sin(((((int)(((360 / (float) (LEDS))) - bkgd_step) % 360) / 360.0f) * 2 * 3.14f) + (6.28f / 3)) + 1));

    for (int x = 0; x < LEDS; x++)
    {
      strip.setPixelColor(x, strip.Color(((bright * red) / 256), ((bright * grn) / 256), ((bright * blu) / 256)));
    }
    strip.show();
}
